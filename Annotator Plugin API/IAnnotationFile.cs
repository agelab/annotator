﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Annotator.TimelineElements;

namespace Annotator.Plugins
{
    /// <summary>
    /// The definition of the annotation file containing all annotation data.
    /// </summary>
    public interface IAnnotationFile
    {
        Dictionary<string, string> SessionKeyValues { get; }
        Dictionary<Keys, IAnnotationKey> AnnotationKeys { get; }
        SortedSet<IAnnotation> Annotations { get; }
        List<ITimelineElement> TimelineElements { get; }
    }

    /// <summary>
    /// The definition of a key used for annotations.
    /// </summary>
    public interface IAnnotationKey
    {
        Keys Key{ get; set; }
        string Description { get; set; }
    }

    /// <summary>
    /// The definition of an annotation event, composed of the key pressed and the time when the event occurred.
    /// </summary>
    public interface IAnnotation : IComparable<IAnnotation>
    {
        IAnnotationKey Key { get; }
        decimal Time { get; }
    }

    /// <summary>
    /// The definition of a TimelineElement, with a type and an offset.
    /// </summary>
    public interface ITimelineElement
    {
        decimal Offset { get; }
        ElementType Type { get; }
        string Filepath { get; }
    }

}
