﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Annotator.Plugins
{
    public interface IPluginHost
    {
        /// <summary>
        /// Writes the specified string to the session log in the main Annotator window.
        /// </summary>
        /// <param name="data">String to display in the session log.</param>
        void AddSessionLogData(string data);

        /// <summary>
        /// Gets the value associated with the given key in the present session, or returns null if the key is not associated with a value.
        /// </summary>
        /// <param name="key">Key whose value should be looked up. Must not be null, empty or whitespace only. Must not contain tab characters.</param>
        /// <returns>The value associated with the key in the current session.</returns>
        string GetSessionKeyValue(string key);

        /// <summary>
        /// Add the given key-value mapping to the present session. Overwrites the existing mapping, if one exists. If the value passed in is null, removes any existing mapping with the same key.
        /// </summary>
        /// <param name="key">The key of the key-value mapping. Must not be null, empty or whitespace only. Must not contain tab characters.</param>
        /// <param name="value">The value of the key-value mapping. If null, removes the existing mapping (if one exists).</param>
        void SetSessionKeyValue(string key, string value);

        /// <summary>
        /// Removes the key-value mapping with the specified key, if one exists.
        /// </summary>
        /// <param name="key">The key of the key-value mapping to remove. Must not be null or empty.</param>
        /// <returns>Returns true if a mapping with the given key existed and was removed; otherwise returns false.</returns>
        bool RemoveSessionKeyValue(string key);

        /// <summary>
        /// Adds a menu item for the plugin with the specified text and callback for click events.
        /// </summary>
        /// <param name="text">Text to add to the menu item.</param>
        /// <param name="callback">Action to be called when the menu item is clicked.</param>
        void AddPluginMenuItem(string text, Action callback);

        /// <summary>
        /// Returns an IEnumerable containing all paths where Annotator will look for missing resource (data, video, audio) files.
        /// </summary>
        IEnumerable<string> GetSearchPaths();
        
        /// <summary>
        /// Adds the specified path to the collection of paths where Annotator looks for missing resource files.
        /// </summary>
        /// <param name="path">The path to add.</param>
        /// <returns>True if the path was added successfully, false otherwise. A return value of false may mean that the path does not exist or that it was already present in the collection.</returns>
        bool AddSearchPath(string path);

        /// <summary>
        /// Returns the path to the plugin's directory, a directory that the plugin may use at its own discretion (e.g. to save config files).
        /// </summary>
        string GetPluginDirectoryPath();

    }
}
