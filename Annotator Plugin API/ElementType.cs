﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Annotator.TimelineElements
{
    public enum ElementType
    {
        VIDEO = 0, CHART = 1, DATA = 2, AUDIO = 3
    }
}
