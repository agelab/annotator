﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Annotator.Plugins
{
    public interface IPlugin
    {
        /// <summary>
        /// Called when the plugin is being initialized.
        /// </summary>
        /// <param name="host">The host interface which allows the plugin to communicate with the host.</param>
        void Initialize(IPluginHost host);

        /// <summary>
        /// Called just before Annotator saves an annotation file.
        /// </summary>
        /// <param name="data">The annotation file data which will be saved. The data will not contain modifications caused by other plugins.</param>
        void OnSavingAnnotations(IAnnotationFile data);

        /// <summary>
        /// Called just after Annotator has saved an annotation file.
        /// </summary>
        /// <param name="data">The annotation file data that was saved.</param>
        /// <param name="filepath">The path of the saved file.</param>
        void OnSavedAnnotations(IAnnotationFile data, string filepath);

        /// <summary>
        /// Called when the Annotator has closed.
        /// </summary>
        void OnApplicationClosed();
    }

    public interface IPluginData
    {
        /// <summary>
        /// The name of the plugin. Must not contain any characters that cannot be used in paths (like /\?* and so on).
        /// </summary>
        string Name { get; }
    }

}
