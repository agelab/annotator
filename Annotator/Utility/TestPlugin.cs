﻿using Annotator.Plugins;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Annotator.Utility
{
    //Uncomment the two lines below to make plugin discoverable and add it to annotator
    //[Export(typeof(IPlugin))]
    //[ExportMetadata("Name", "Test plugin")]
    class TestPlugin : IPlugin
    {
        #region IPlugin Members

        private IPluginHost host;

        public void Initialize(IPluginHost host)
        {
            this.host = host;
            MessageBox.Show("Initialized test plugin!");
            host.AddPluginMenuItem("Make test message", () => MessageBox.Show("Test message!"));
        }

        public void OnSavingAnnotations(IAnnotationFile data)
        {
            host.SetSessionKeyValue("test-saving", "OK");
        }

        public void OnSavedAnnotations(IAnnotationFile data, string filepath)
        {
            host.SetSessionKeyValue("save-path", filepath);
        }

        public void OnApplicationClosed()
        {
            //no-op
        }

        #endregion
    }
}
