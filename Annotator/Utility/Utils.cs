﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Annotator.Utility
{
    public static class Utils
    {
        public const decimal EPS = 1e-9m;

        public static void SetInvariantCulture()
        {
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
            System.Threading.Thread.CurrentThread.CurrentCulture = culture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
        }

        public static void ApplyToControlAndChildren(Control f, Action<Control> action) //DragEventHandler enterDrag, DragEventHandler dragDrop)
        {
            action(f);
            foreach (Control c in f.Controls)
            {
                ApplyToControlAndChildren(c,action); //enterDrag,dragDrop);
            }
        }

        public static void AppendTwoDigitRepresentation(StringBuilder sb,int inp)
        {
            if (inp < 10)
            {
                sb.Append("0");
            }
            sb.Append(inp);
        }

        public static decimal RoundToTwoDecimals(decimal db)
        {
            try
            {
                return Math.Round(db * 100) / 100;
            } catch (OverflowException) {
                return db;
            }
        }

        public static StringBuilder CreateTimeString(decimal time)
        {
            int min = (int)(time / 60);
            int sec = ((int)time) % 60;
            int milli = ((int)Math.Round(time * 100)) % 100;

            StringBuilder sb = new StringBuilder();
            Utils.AppendTwoDigitRepresentation(sb, min);
            sb.Append(":");
            Utils.AppendTwoDigitRepresentation(sb, sec);
            sb.Append(".");
            Utils.AppendTwoDigitRepresentation(sb, milli);
            return sb;
        }

        public static StringBuilder CreateSecondStringNoUnit(decimal time)
        {
            int sec = (int)time;
            int milli = ((int)Math.Round(time * 100)) % 100;

            StringBuilder sb = new StringBuilder(sec.ToString());
            sb.Append(".");
            Utils.AppendTwoDigitRepresentation(sb, milli);
            return sb;
        }

        public static StringBuilder CreateSecondString(decimal time)
        {
            int sec = (int)time;
            int milli = ((int)Math.Round(time * 100)) % 100;

            StringBuilder sb = new StringBuilder(sec.ToString());
            sb.Append(".");
            Utils.AppendTwoDigitRepresentation(sb, milli);
            sb.Append("s");
            return sb;
        }
    }
}
