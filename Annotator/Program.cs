﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Annotator.Forms;
using Annotator.Utility;

namespace Annotator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Utils.SetInvariantCulture();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
