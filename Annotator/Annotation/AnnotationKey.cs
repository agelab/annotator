﻿using Annotator.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Annotator.Annotations
{
    public class AnnotationKey : IAnnotationKey
    {
        private Keys key;
        private string description;

        public const char SEPARATOR = '\t';

        /// <summary>
        /// Initializes a new instance of the <see cref="AnnotationKey"/> class.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="description">The description.</param>
        public AnnotationKey(Keys key, string description)
        {
            this.key = key;
            if (string.IsNullOrWhiteSpace(description))
            {
                throw new ArgumentException("Description may not be null, empty or only whitespace.", "description");
            }
            this.description = description;
        }

        /// <summary>
        /// Gets the key.
        /// </summary>
        public Keys Key
        {
            get
            {
                return key;
            }
            set
            {
                key = value;
            }
        }

        /// <summary>
        /// Gets the description.
        /// </summary>
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    value = "<empty>";
                }
                description = value;
            }
        }

        public static AnnotationKey CreateFromString(string inp)
        {
            String[] args = inp.Split(SEPARATOR);
            return new AnnotationKey((Keys)int.Parse(args[args.Length - 2]), args[args.Length - 1]);
        }

        public override string ToString()
        {
            return ((int)key).ToString() + SEPARATOR + description;
        }

    }
}
