﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.ComponentModel;
using Annotator.Controllers;
using Annotator.Data;
using Annotator.TimelineElements;
using System.Runtime.CompilerServices;
using Annotator.Plugins;

[assembly: InternalsVisibleTo("AnnotatorTest")]

namespace Annotator.Annotations
{
    
    public class Annotator
    {
        private readonly Dictionary<string, string> sessionKeyValues = new Dictionary<string, string>();
        private readonly Dictionary<string, KeyValuePair<string, string>> sessionKeyIds = new Dictionary<string, KeyValuePair<string, string>>();
        private readonly Dictionary<Keys, AnnotationKey> annotationKeys = new Dictionary<Keys, AnnotationKey>();
        private readonly SortedSet<Annotation> annotations = new SortedSet<Annotation>();
        private readonly LinkedList<Annotation> annotationOrder = new LinkedList<Annotation>();
        private readonly BindingSource bindingSource = new BindingSource();

        private const char SEPARATOR = '\t';

        private const string ANNOTATION_KEY_CATEGORY = "AnnotationKeys";
        private const string ANNOTATION_KEY_PROPERTYBASE = "annotationKey";

        private static readonly string headerRow = String.Format("KeyPress{0}Description{0}AbsoluteTime", SEPARATOR);

        public void AddDefaultAnnotationKeys()
        {
            var ky = new Keys[] { Keys.F1, Keys.F2, Keys.F3, Keys.F4, Keys.F5, Keys.F6, Keys.F7, Keys.F8, Keys.F9, Keys.F10, Keys.F11, Keys.F12 };
            foreach(var k in ky)
            {
                if(!annotationKeys.ContainsKey(k))
                {
                    annotationKeys.Add(k, new AnnotationKey(k, "The " + k.ToString() + " key"));
                }
            }
        }

        public Annotation LastAddedAnnotation
        {
            get
            {
                if (annotations.Count > 0)
                {
                    return annotationOrder.Last.Value;
                }
                else
                {
                    return null;
                }
            }
        }

        public bool RemoveLastAddedAnnotation()
        {
            if (annotations.Count > 0)
            {
                bool success = annotations.Remove(annotationOrder.Last.Value);
                annotationOrder.RemoveLast();
                return success;
            }
            return false;
        }

        public void ClearAnnotations()
        {
            annotations.Clear();
            annotationOrder.Clear();
        }

        public void ClearAnnotationKeys()
        {
            annotationKeys.Clear();
        }

        public AnnotationKey GetAnnotationKey(Keys key)
        {
            return annotationKeys[key];
        }

        public void AddAnnotationKey(Keys key, AnnotationKey ak)
        {
            if (!annotationKeys.ContainsKey(key))
            {
                annotationKeys.Add(key, ak);
            }
        }

        public void RemoveAnnotationKey(Keys key)
        {
            if(annotationKeys.ContainsKey(key))
            {
                annotationKeys.Remove(key);
            }
        }

        public void ChangeDescription(Keys key, string description)
        {
            if (annotationKeys.ContainsKey(key))
            {
                annotationKeys[key].Description = description;
            }
        }

        public Dictionary<Keys,AnnotationKey>.KeyCollection ReservedKeys
        {
            get
            {
                return annotationKeys.Keys;
            }
        }

        public bool AnnotateKeyPress(Keys key,decimal time)
        {
            bool res = annotationKeys.ContainsKey(key);
            if (res)
            {
                Annotation ann = new Annotation(annotationKeys[key], time);
                annotations.Add(ann);
                annotationOrder.AddLast(ann);
            }
            return res;
        }

        internal string CreateAnnotationList()
        {
            StringBuilder sb = new StringBuilder();
            foreach(var x in annotations)
            {
                sb.AppendLine(x.ToString());
            }
            return sb.ToString();
        }

        public void SaveProperties(IniFile file)
        {
            int i = 0;
            foreach(AnnotationKey value in annotationKeys.Values)
            {
                file.WriteValue(ANNOTATION_KEY_CATEGORY, ANNOTATION_KEY_PROPERTYBASE + i.ToString(), CreateAnnotationKeyString(value));
                i++;
            }
        }

        public bool ApplyProperties(IniFile file, MainController mainController, List<string> errorMessages)
        {
            int i = 0;
            bool hasMoreValues = true;
            bool hasErrors = false;
            String value;
            while (hasMoreValues)
            {
                value = file.ReadValue(ANNOTATION_KEY_CATEGORY, ANNOTATION_KEY_PROPERTYBASE + i.ToString());
                i++;
                if (value == "")
                {
                    hasMoreValues = false;
                }
                else
                {
                    AnnotationKey key;
                    try
                    {
                        key = ReadAnnotationKeyString(value);
                        if (annotationKeys.ContainsKey(key.Key))
                        {
                            if (annotationKeys[key.Key].Description != key.Description)
                            {
                                hasErrors = true;
                                errorMessages.Add("Could not add annotation key " + key.Key + " with description \"" + key.Description +
                                    "\" because the key is already in use for an annotation with description \"" + annotationKeys[key.Key].Description + "\"");
                            }
                        }
                        else
                        {
                            annotationKeys.Add(key.Key, key);
                            mainController.AddAnnotationToMainForm(key.Key, key.Description);
                        }
                    }
                    catch (InvalidOperationException)
                    {
                        hasErrors = true;
                        errorMessages.Add("Could not parse annotation record \"" + value + "\"");
                    }
                }
            }
            return !hasErrors;
        }

        private static String CreateAnnotationKeyString(AnnotationKey key)
        {
            return String.Format("{0}{1}{2}", (int)key.Key, SEPARATOR, key.Description);
        }

        private AnnotationKey ReadAnnotationKeyString(String record)
        {
            String[] parts = record.Split(SEPARATOR);
            int keyValue;
            AnnotationKey key;

            if (parts.Length >= 2)
            {
                keyValue = int.Parse(parts[0]);
                key = new AnnotationKey((Keys)keyValue, parts[1]);
                return key;
            }
            else
            {
                throw new InvalidOperationException("Cannot parse annotation record \"" + record + "\"");
            }
        }

        public AnnotationFile MakeAnnotationFile(Timeline timeline, DataFile.AnnotationRowExtractor extractor = null)
        {
            Dictionary<Keys, IAnnotationKey> ank = new Dictionary<Keys,IAnnotationKey>();
            foreach(var x in annotationKeys)
            {
                ank.Add(x.Key, x.Value);
            }
            return new AnnotationFile(new Dictionary<string,string>(sessionKeyValues), ank, new SortedSet<IAnnotation>(annotations), timeline, extractor);
        }

        public class AnnotationFile : IAnnotationFile
        {
            private readonly Dictionary<string, string> sessionKeyValues;
            private readonly Dictionary<Keys, IAnnotationKey> annotationKeys;
            private readonly SortedSet<IAnnotation> annotations;
            private readonly List<ITimelineElement> timelineElements;
            private readonly DataFile.AnnotationRowExtractor extractor;

            private const char SEPARATOR = '\t';
            private const char SESSION_KEY_VALUES_SECTOR_MARKER = '*';

            internal AnnotationFile(Dictionary<string, string> sessionKeyValues, Dictionary<Keys, IAnnotationKey> annotationKeys, SortedSet<IAnnotation> annotations, List<ITimelineElement> timelineElements, DataFile.AnnotationRowExtractor extractor = null)
            {
                this.sessionKeyValues = sessionKeyValues;
                this.annotationKeys = annotationKeys;
                this.annotations = annotations;
                this.timelineElements = timelineElements;
                this.extractor = extractor;
            }

            internal AnnotationFile(Dictionary<string, string> sessionKeyValues, Dictionary<Keys, IAnnotationKey> annotationKeys, SortedSet<IAnnotation> annotations, Timeline timeline, DataFile.AnnotationRowExtractor extractor = null)
                : this(sessionKeyValues, annotationKeys, annotations, timeline.Children.Cast<ITimelineElement>().ToList(), extractor)
            {

            }

            public Dictionary<string, string> SessionKeyValues
            {
                get
                {
                    return sessionKeyValues;
                }
            }

            public Dictionary<Keys, IAnnotationKey> AnnotationKeys
            {
                get
                {
                    return annotationKeys;
                }
            }

            public SortedSet<IAnnotation> Annotations
            {
                get
                {
                    return annotations;
                }
            }

            public List<ITimelineElement> TimelineElements
            {
                get
                {
                    return timelineElements;
                }
            }

            private void WriteFileList(StreamWriter sw)
            {
                foreach (var x in timelineElements)
                {
                    sw.WriteLine(x.ToString());
                }
            }

            private static List<ITimelineElement> ReadFileList(StreamReader sr)
            {
                List<ITimelineElement> res = new List<ITimelineElement>();
                string s;
                while ((s = sr.ReadLine().Trim()) != "")
                {
                    try
                    {
                        res.Add(TimelineElementBuilder.CreateFromString(s));
                    }
                    catch (ArgumentException) { }
                }
                return res;
            }

            private void WriteKeyList(StreamWriter sw)
            {
                foreach (var x in annotationKeys)
                {
                    sw.WriteLine(x.Value.ToString());
                }
            }

            private static Dictionary<Keys, IAnnotationKey> ReadKeyList(StreamReader sr)
            {
                Dictionary<Keys, IAnnotationKey> res = new Dictionary<Keys, IAnnotationKey>();
                string s;
                while ((s = sr.ReadLine().Trim()) != "")
                {
                    AnnotationKey key = AnnotationKey.CreateFromString(s);
                    res.Add(key.Key,key);
                }
                return res;
            }

            public static AnnotationFile ReadFromFile(string inputFile)
            {
                using (StreamReader sr = new StreamReader(inputFile))
                {
                    Dictionary<string, string> sessionKeyValues = ReadSessionKeyValues(sr);
                    List<ITimelineElement> timelineElements = ReadFileList(sr);
                    Dictionary<Keys, IAnnotationKey> keys = ReadKeyList(sr);
                    char c = (char)sr.Peek();
                    if (!char.IsNumber(c))
                    {
                        sr.ReadLine(); //ignore header row
                    }
                    SortedSet<IAnnotation> annotations = ReadAnnotationList(sr);
                    return new AnnotationFile(sessionKeyValues, keys, annotations, timelineElements);
                }
            }

            private static Dictionary<string, string> ReadSessionKeyValues(StreamReader sr)
            {
                var sessionKeyValues = new Dictionary<string, string>();
                if((char)sr.Peek() == SESSION_KEY_VALUES_SECTOR_MARKER)
                {
                    sr.ReadLine();
                    string s = sr.ReadLine().Trim();
                    int index;
                    while(s.Length > 1 || s[0] != SESSION_KEY_VALUES_SECTOR_MARKER)
                    {
                        index = s.IndexOf(SEPARATOR);
                        if(index > -1)
                        {
                            sessionKeyValues.Add(s.Substring(0, index), s.Substring(index+1));
                        }
                        s = sr.ReadLine().Trim();
                    }
                }
                return sessionKeyValues;
            }

            public void WriteToFile(string outputFile)
            {
                if (extractor == null)
                {
                    using (StreamWriter sw = new StreamWriter(outputFile))
                    {
                        sw.WriteLine(SESSION_KEY_VALUES_SECTOR_MARKER);
                        WriteSessionKeyValues(sw);
                        sw.WriteLine(SESSION_KEY_VALUES_SECTOR_MARKER);
                        WriteFileList(sw);
                        sw.WriteLine();
                        WriteKeyList(sw);
                        sw.WriteLine();
                        sw.WriteLine(headerRow);
                        WriteAnnotationList(sw);
                    }
                }
                else
                {
                    foreach (var x in annotations)
                    {
                        extractor.AnnotateTime(x.Time);
                    }

                    using (StreamWriter sw = new StreamWriter(outputFile))
                    {
                        sw.Write(SESSION_KEY_VALUES_SECTOR_MARKER);
                        WriteSessionKeyValues(sw);
                        sw.WriteLine(SESSION_KEY_VALUES_SECTOR_MARKER);
                        WriteFileList(sw);
                        sw.WriteLine();
                        WriteKeyList(sw);
                        sw.WriteLine();
                        sw.WriteLine(extractor.GetHeaderRow(headerRow));
                        WriteAnnotationList(sw);
                    }
                }
            }

            private void WriteSessionKeyValues(StreamWriter sw)
            {
                foreach(var x in sessionKeyValues)
                {
                    sw.WriteLine("{0}{1}{2}", x.Key, SEPARATOR, x.Value);
                }
            }

            private static SortedSet<IAnnotation> ReadAnnotationList(StreamReader sr)
            {
                SortedSet<IAnnotation> annotations = new SortedSet<IAnnotation>();
                while (!sr.EndOfStream)
                {
                    annotations.Add(Annotation.CreateFromString(sr.ReadLine()));
                }
                return annotations;
            }

            private void WriteAnnotationList(StreamWriter sw)
            {
                if (extractor == null)
                {
                    foreach (var x in annotations)
                    {
                        sw.WriteLine(x.ToString());
                    }
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    List<string> ls = new List<string>();
                    foreach (var x in annotations)
                    {
                        sb.Append(x.ToString());
                        sb.Append(SEPARATOR);
                        ls.Add(sb.ToString());
                        sb.Clear();
                    }

                    int i = 0;
                    foreach (var x in extractor)
                    {
                        sw.Write(ls[i]);
                        sw.Write(SEPARATOR);
                        sw.WriteLine(x);
                        i++;
                    }
                }
            }

        }

        public void RemoveAnnotation(Annotation annotation)
        {
            annotations.Remove(annotation);
            annotationOrder.Remove(annotation);
        }

        public BindingSource BindingSource
        {
            get
            {
                return bindingSource;
            }
        }

        public string GetSessionKeyValue(string key)
        {
            if(sessionKeyValues.ContainsKey(key))
            {
                return sessionKeyValues[key];
            } else {
                return null;
            }
        }

        public void SetSessionKeyValue(string key, string value)
        {
            if(key == null || string.IsNullOrWhiteSpace(key) || key.Contains(SEPARATOR))
            {
                throw new ArgumentException(string.Format("Illegal key used: \"{0}\"", key == null ? "null" : key), "key");
            }
            if(sessionKeyIds.ContainsKey(key))
            {
                RemoveSessionKeyValue(key);
            }
            var x = new KeyValuePair<string, string>(key, value);
            sessionKeyIds.Add(key, x);
            sessionKeyValues[key] = value;
            bindingSource.Add(x);
        }

        public bool RemoveSessionKeyValue(string key)
        {
            if(sessionKeyValues.Remove(key))
            {
                bindingSource.Remove(sessionKeyIds[key]);
                sessionKeyIds.Remove(key);
                return true;
            }
            return false;
        }

        public bool IsValidSessionKey(string key)
        {
            return !(string.IsNullOrWhiteSpace(key) || key.Contains(SEPARATOR) || sessionKeyValues.ContainsKey(key));
        }

        public void ClearSessionKeyValues()
        {
            sessionKeyIds.Clear();
            sessionKeyValues.Clear();
            bindingSource.Clear();
        }
    }
}
