﻿using Annotator.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Annotator.Annotations
{
    public class Annotation : IAnnotation
    {
        private readonly AnnotationKey key;
        private readonly decimal time;

        public const char SEPARATOR = '\t';

        public Annotation(AnnotationKey key, decimal time)
        {
            this.key = key;
            this.time = time;
        }

        public IAnnotationKey Key
        {
            get
            {
                return key;
            }
        }

        public decimal Time
        {
            get
            {
                return time;
            }
        }

        public int CompareTo(IAnnotation other)
        {
            int res = Comparer<decimal>.Default.Compare(time, other.Time);
            if (res == 0)
            {
                res = Comparer<int>.Default.Compare((int)key.Key, (int)other.Key.Key);
                if (res == 0 && key.Key == Keys.NoName)
                {
                    res = Comparer<String>.Default.Compare(key.Description, other.Key.Description);
                }
            }
            return res;
        }

        public static Annotation CreateFromString(string inp)
        {
            int x = inp.IndexOf(SEPARATOR,inp.IndexOf(SEPARATOR)+1);
            int y = inp.IndexOf(SEPARATOR,x+1);
            if (y == -1)
            {
                return new Annotation(AnnotationKey.CreateFromString(inp.Substring(0, x)), decimal.Parse(inp.Substring(x + 1, inp.Length - x - 1)));
            }
            else
            {
                return new Annotation(AnnotationKey.CreateFromString(inp.Substring(0, x)), decimal.Parse(inp.Substring(x + 1, y - x - 1)));
            }
        }

        public override string ToString()
        {
            return key.ToString() + SEPARATOR + time.ToString();
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Annotation p = obj as Annotation;
            if ((object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (key.Key == p.key.Key) && (time == p.time);
        }

        public bool Equals(IAnnotation p)
        {
            // If parameter is null return false:
            if ((object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (key.Key == p.Key.Key) && (time == p.Time);
        }

        public override int GetHashCode()
        {
            return key.GetHashCode() ^ time.GetHashCode();
        }

    }
}
