﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DirectShowLib;
using Annotator.Controllers;

namespace Annotator.TimelineElements
{
    public class AudioElement : TimelineElement
    {
        private readonly IGraphBuilder graph;
        private readonly IMediaControl mediaControl;
        private readonly IMediaPosition mediaPosRate;

        public class AudioElementBuilder : TimelineElementBuilder
        {

            internal AudioElementBuilder(string path, decimal offset)
                : base(ElementType.AUDIO, path)
            {
                Offset = offset;
            }

            public static TimelineElementBuilder ParseString(String[] args)
            {
                return new AudioElementBuilder(args[1], decimal.Parse(args[2]));
            }

            public override void BuildAndAdd(MainController controller)
            {
                controller.AddAudio(filepath, offset);
            }

        }

        public AudioElement(string path)
            : this(path, 0m)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AudioElement"/> class.
        /// </summary>
        /// <param name="path">The file path of the audio file.</param>
        public AudioElement(string path, decimal offset) : base(ElementType.AUDIO, path)
        {
            int hr;
            if (String.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("Invalid argument: path", "path");
            }
            filepath = path;
            Offset = offset;
            graph = (IGraphBuilder) new FilterGraph();
            hr = graph.RenderFile(filepath, null);
            DsError.ThrowExceptionForHR(hr);

            mediaControl = (IMediaControl)graph;
            mediaPosRate = (IMediaPosition)graph;
        }

        public override TimelineElementBuilder CreateBuilder()
        {
            return new AudioElementBuilder(filepath, offset);
        }

        protected override void OnPlaySpeedChanged()
        {
            mediaPosRate.put_Rate((double)playSpeed);
        }

        protected override void OnOffsetChanged(decimal dOffset)
        {
            double pos;
            int hr;

            hr = mediaPosRate.get_CurrentPosition(out pos);
            DsError.ThrowExceptionForHR(hr);

            hr = mediaPosRate.put_CurrentPosition(pos + (double)dOffset);
            DsError.ThrowExceptionForHR(hr);
        }

        public override void Play()
        {
            if (mediaControl.Run() >= 0)
                base.Play();
            else
                throw new InvalidOperationException(string.Format("Could not play file: {0}", filepath));
        }

        public override void Pause()
        {
            if (mediaControl.Pause() >= 0)
                base.Pause();
            else
                throw new InvalidOperationException(string.Format("Could not pause file: {0}", filepath));
        }

        public override decimal StepSize
        {
            get
            {
                return 0m;
            }
        }

        public override decimal EndPosition
        {
            get
            {
                double val;
                int hr = mediaPosRate.get_Duration(out val);
                DsError.ThrowExceptionForHR(hr);
                return offset + ((decimal)val);
            }
        }

        public override decimal CurrentPosition
        {
            get
            {
                double val;
                int hr = mediaPosRate.get_CurrentPosition(out val);
                DsError.ThrowExceptionForHR(hr);
                return offset + ((decimal)val);
            }
            set
            {
                int hr;
                if (value < offset)
                {
                    throw new ArgumentOutOfRangeException("VideoElement.CurrentPosition", value + offset, "value must be greater than offset.");
                }
                else if (value <= EndPosition)
                {
                    hr = mediaPosRate.put_CurrentPosition((double)value);
                    DsError.ThrowExceptionForHR(hr);
                }
            }
        }

        public override string GetUniqueID()
        {
            return filepath;
        }

        public override void Update(decimal time)
        {
            // no-op
        }

        public override string ToString()
        {
            return String.Format("{1}{0}{2}{0}{3}", SEPARATOR, (int)type, filepath, Offset);
        }

        protected override void Dispose(bool disposing)
        {
            Pause();

            base.Dispose(disposing);
            int hr = graph.Abort();
            DsError.ThrowExceptionForHR(hr);
        }

    }
}
