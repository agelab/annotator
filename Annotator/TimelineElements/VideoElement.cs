﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DirectShowLib;
using Annotator.Controllers;

namespace Annotator.TimelineElements
{
    public class VideoElement : TimelineElement
    {
        private readonly Control drawSurface;
        private readonly IGraphBuilder graph;
        private readonly IMediaControl mediaControl; 
        private readonly IVideoFrameStep frameStep;
        private readonly IMediaPosition mediaPosRate;
        private readonly IBasicVideo basicVideo;
        private readonly IVideoWindow videoWindow;

        private const int WMGraphNotify = 0x0400 + 13;

        public class VideoElementBuilder : TimelineElementBuilder
        {

            internal VideoElementBuilder(string path, decimal offset)
                : base(ElementType.VIDEO, path)
            {
                Offset = offset;
            }

            public static TimelineElementBuilder ParseString(String[] args)
            {
                return new VideoElementBuilder(args[1], decimal.Parse(args[2]));
            }

            public override void BuildAndAdd(MainController controller)
            {
                controller.AddVideo(filepath, offset);
            }

        }

        public VideoElement(string path, Control surface)
            : this(path, 0m, surface)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VideoElement"/> class, and displays it on
        /// the given control. Sets the control's Tag property to the filepath of the video.
        /// </summary>
        /// <param name="path">The file path of the video.</param>
        /// <param name="surface">The surface to play the video on.</param>
        public VideoElement(string path, decimal offset, Control surface) : base(ElementType.VIDEO, path)
        {
            int hr;
            if (String.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("Invalid argument: path", "path");
            }
            if (surface == null)
            {
                throw new ArgumentNullException("Invalid argument: surface", "surface");
            }
            filepath = path;
            Offset = offset;
            drawSurface = surface;
            drawSurface.Tag = filepath;
            graph = (IGraphBuilder) new FilterGraph();
            hr = graph.RenderFile(filepath, null);
            DsError.ThrowExceptionForHR(hr);

            mediaControl = (IMediaControl)graph;
            mediaPosRate = (IMediaPosition)graph;
            frameStep = (IVideoFrameStep)graph;
            basicVideo = (IBasicVideo)graph;
            videoWindow = (IVideoWindow)graph;

            hr = ((IMediaEventEx)graph).SetNotifyWindow(drawSurface.Handle, WMGraphNotify, IntPtr.Zero);
            DsError.ThrowExceptionForHR(hr);

            hr = videoWindow.put_Owner(drawSurface.Handle);
            DsError.ThrowExceptionForHR(hr);

            hr = this.videoWindow.put_WindowStyle(WindowStyle.Child | WindowStyle.ClipSiblings | WindowStyle.ClipChildren);
            DsError.ThrowExceptionForHR(hr);

            hr = this.videoWindow.SetWindowPosition(0, 0, drawSurface.Width, drawSurface.Height);
            DsError.ThrowExceptionForHR(hr);
            
            drawSurface.Resize+=new EventHandler(drawSurface_MovedResized);
            drawSurface.Move+=new EventHandler(drawSurface_MovedResized);
        }

        public override TimelineElementBuilder CreateBuilder()
        {
            return new VideoElementBuilder(filepath, offset);
        }

        private void drawSurface_MovedResized(object sender, EventArgs e)
        {
            int hr;
            hr = this.videoWindow.SetWindowPosition(0, 0, drawSurface.Width, drawSurface.Height);
            DsError.ThrowExceptionForHR(hr);
        }

        protected override void OnPlaySpeedChanged()
        {
            mediaPosRate.put_Rate((double)playSpeed);
        }

        protected override void OnOffsetChanged(decimal dOffset)
        {
            double pos;
            int hr;

            hr = mediaPosRate.get_CurrentPosition(out pos);
            DsError.ThrowExceptionForHR(hr);

            hr = mediaPosRate.put_CurrentPosition(pos+(double)dOffset);
            DsError.ThrowExceptionForHR(hr);
        }

        public Control DrawSurface
        {
            get
            {
                return drawSurface;
            }
        }

        public override void Play()
        {
            if (mediaControl.Run() >= 0)
                base.Play();
            else
                throw new InvalidOperationException(string.Format("Could not play file: {0}", filepath));
        }

        public override void Pause()
        {
            if (mediaControl.Pause() >= 0)
                base.Pause();
            else
                throw new InvalidOperationException(string.Format("Could not pause file: {0}", filepath));
        }

        public override decimal StepSize
        {
            get
            { 
                double val;
                int hr = basicVideo.get_AvgTimePerFrame(out val);
                DsError.ThrowExceptionForHR(hr);
                return (decimal)val;
            }
        }

        public override decimal EndPosition
        {
            get 
            {
                double val;
                int hr = mediaPosRate.get_Duration(out val);
                DsError.ThrowExceptionForHR(hr);
                return offset+((decimal)val);
            }
        }

        public override decimal CurrentPosition
        {
            get
            {
                double val;
                int hr = mediaPosRate.get_CurrentPosition(out val);
                DsError.ThrowExceptionForHR(hr);
                return offset + ((decimal)val);
            }
            set
            {
                int hr;
                if (value < offset)
                {
                    throw new ArgumentOutOfRangeException("VideoElement.CurrentPosition", value + offset, "value must be greater than offset.");
                }
                else if (value <= EndPosition)
                {
                    hr = mediaPosRate.put_CurrentPosition((double)value);
                    DsError.ThrowExceptionForHR(hr);
                }
            }
        }

        public int VideoHeight
        {
            get
            {
                int res,hr;
                hr = basicVideo.get_VideoHeight(out res);
                DsError.ThrowExceptionForHR(hr);
                return res;
            }
        }

        public int VideoWidth
        {
            get
            {
                int res, hr;
                hr = basicVideo.get_VideoWidth(out res);
                DsError.ThrowExceptionForHR(hr);
                return res;
            }
        }

        public override string GetUniqueID()
        {
            return filepath;
        }

        public override void Update(decimal time)
        {
            // no-op
        }

        public override string ToString()
        {
            return String.Format("{1}{0}{2}{0}{3}", SEPARATOR, (int)type, filepath, Offset);
        }

        protected override void Dispose(bool disposing)
        {
            Pause();

            base.Dispose(disposing);
            if (disposing)
            {
                drawSurface.Resize -= drawSurface_MovedResized;
                drawSurface.Move -= drawSurface_MovedResized;
                drawSurface.Dispose();
            }
            int hr = graph.Abort();
            DsError.ThrowExceptionForHR(hr);
        }
    }
}