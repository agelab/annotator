﻿using OxyPlot;
using OxyPlot.Annotations;
using OxyPlot.Axes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Annotator.Utility;
using Annotator.Controllers;
using OxyPlot.Series;
using OxyPlot.WindowsForms;

namespace Annotator.TimelineElements
{
    public class ChartElement : TimelineElement
    {

        public class ChartElementBuilder : TimelineElementBuilder
        {
            private readonly string timeColumn;
            private readonly string dataColumn;
            public ChartElementBuilder(string filepath, string timeColumn, string dataColumn, decimal offset)
                : base(ElementType.CHART, filepath)
            {
                this.timeColumn = timeColumn;
                this.dataColumn = dataColumn;
                Offset = offset;
            }

            public override void BuildAndAdd(MainController controller)
            {
                controller.AddChart(filepath, timeColumn, dataColumn, offset);
            }

            public static ChartElementBuilder ParseString(string[] values)
            {
                return new ChartElementBuilder(values[1], values[2], values[3], decimal.Parse(values[4]));
            }
        }

        private const int DEFAULT_VIEW_RADIUS = 5;

        public ChartElement(string filepath, string timeColumn, string dataColumn) : base(ElementType.CHART, filepath)
        {
            string title = String.Format("{0} vs {1}", dataColumn, timeColumn);
            string subtitle = System.IO.Path.GetFileName(filepath);
            this.chartModel = new PlotModel(title, subtitle);

            yAxis = new LinearAxis();
            yAxis.MinimumPadding = 0.1;
            yAxis.MaximumPadding = 0.1;
            yAxis.Title = dataColumn;
            yAxis.IsZoomEnabled = false;
            yAxis.IsPanEnabled = false;
            this.chartModel.Axes.Add(yAxis);

            xAxis = new LinearAxis();
            xAxis.Position = AxisPosition.Bottom;
            xAxis.Title = timeColumn;
            xAxis.IsZoomEnabled = false;
            xAxis.IsPanEnabled = true;
            this.chartModel.Axes.Add(xAxis);

            this.startAnnotation = new LineAnnotation();
            this.startAnnotation.Type = LineAnnotationType.Vertical;
            this.startAnnotation.X = 0;
            this.startAnnotation.Selectable = false;
            this.startAnnotation.LineStyle = LineStyle.Dot;
            this.startAnnotation.Text = "data start";
            this.chartModel.Annotations.Add(this.startAnnotation);

            this.endAnnotation = new LineAnnotation();
            this.endAnnotation.Type = LineAnnotationType.Vertical;
            this.endAnnotation.Selectable = false;
            this.endAnnotation.LineStyle = LineStyle.Dot;
            this.endAnnotation.Text = "data end";
            this.chartModel.Annotations.Add(this.endAnnotation);

            this.positionMarker = new LineAnnotation();
            this.positionMarker.Type = LineAnnotationType.Vertical;
            this.positionMarker.X = 0;
            this.positionMarker.Selectable = false;
            this.positionMarker.LineStyle = LineStyle.Solid;
            this.positionMarker.StrokeThickness *= 2;
            this.chartModel.Annotations.Add(this.positionMarker);

            Plot chart = new Plot() {
                KeyboardPanHorizontalStep = 0.1D,
                KeyboardPanVerticalStep = 0.1D,
                Name = title,
                PanCursor = System.Windows.Forms.Cursors.Hand,
                ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE,
                ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE,
                ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS,
                AllowDrop = true,
                Model = this.chartModel
            };
            Chart = chart;

            this.filepath = filepath;
            this.timeColumn = timeColumn;
            this.dataColumn = dataColumn;
        }

        public string DataColumnName
        {
            get
            {
                return this.dataColumn;
            }
        }

        public override TimelineElementBuilder CreateBuilder()
        {
            return new ChartElementBuilder(filepath, timeColumn, dataColumn, offset);
        }

        private readonly LineAnnotation startAnnotation, endAnnotation, positionMarker;
        private readonly PlotModel chartModel;
        private readonly Axis xAxis, yAxis;
        private decimal endPosition = 0, currentPosition = 0;
        private readonly string timeColumn, dataColumn;

        public Plot Chart
        {
            get;
            private set;
        }

        public void AddDataLine(String name, List<decimal> times, List<decimal> values)
        {
            if (times.Count != values.Count)
            {
                throw new ArgumentException(string.Format("value count mismatch: {0} vs {1}", times.Count, values.Count));
            }

            endPosition = Math.Max(endPosition, times[times.Count-1]);
            this.endAnnotation.X = (double)endPosition;

            var series = new LineSeries(this.dataColumn);
            for (int i = 0; i < times.Count; i++)
            {
                series.Points.Add(new DataPoint((double)times[i], (double)values[i]));
            }
            series.Selectable = false;
            series.Smooth = false;
            this.chartModel.Series.Add(series);
        }

        public override decimal StepSize
        {
            get { return 0; }
        }

        public override void Update(decimal time)
        {
            CurrentPosition = time;
        }

        public override decimal CurrentPosition
        {
            get
            {
                return currentPosition + Offset;
            }
            set
            {
                var oldPos = currentPosition;
                currentPosition = value - Offset;
                this.xAxis.IsZoomEnabled = true;
                this.xAxis.Zoom((double)currentPosition - DEFAULT_VIEW_RADIUS, (double)currentPosition + DEFAULT_VIEW_RADIUS);
                // this.xAxis.Pan((double)(currentPosition - oldPos));
                this.positionMarker.X = (double)currentPosition;
                this.Chart.InvalidatePlot(false);
            }
        }

        public override decimal EndPosition
        {
            get
            {
                return endPosition;
            }
        }

        protected override void OnPlaySpeedChanged()
        {
            // no-op
        }

        public override string GetUniqueID()
        {
            StringBuilder sb = new StringBuilder("[Chart='");
            bool comma = false;
            foreach (Axis axis in this.chartModel.Axes)
            {
                if(comma) sb.Append(',');
                sb.Append(axis.Title);
                comma = true;
            }
            sb.Append("']");
            return sb.ToString();
        }

        public override string ToString()
        {
            return String.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}", SEPARATOR, (int)type, filepath, timeColumn, dataColumn, Offset);
        }
    }
}
