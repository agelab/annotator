﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.ComponentModel;
using Annotator.Utility;

namespace Annotator.TimelineElements
{
    public class Timeline
    {

        public const decimal DEFAULT_RATE_STEP = 0.1m;
        public const decimal DEFAULT_JUMP_LENGTH = 1m;

        private readonly Dictionary<String, TimelineElement> elements = new Dictionary<String, TimelineElement>();
        private decimal playSpeed = TimelineElement.DEFAULT_PLAY_SPEED;
        private bool paused = TimelineElement.DEFAULT_PAUSED;
        private decimal startOffset=decimal.MinValue;
        private decimal currentRelPosition = 0;
        private Stopwatch stp = new Stopwatch();
        private decimal endPos=decimal.MinValue;
        private decimal stepSize = decimal.MinValue;

        public void AddElement(TimelineElement elem)
        {
            string id = elem.GetUniqueID();
            if(!elements.ContainsKey(id))
            {
                elements.Add(id,elem);
                if (decimal.MinValue == startOffset)
                {
                    startOffset = elem.Offset;
                    endPos = elem.EndPosition;
                    stepSize = elem.StepSize;
                }
                else
                {
                    startOffset = Math.Max(startOffset, elem.Offset);
                    endPos = Math.Max(endPos, elem.EndPosition);
                    stepSize = Math.Max(stepSize, elem.StepSize);
                }
                elem.Play();
                elem.Pause();
                elem.CurrentPosition = CurrentPosition;
                elem.OffsetChanged += new EventHandler(elem_OffsetChanged);
            }
        }

        public void AddElementRange(params TimelineElement[] elems)
        {
            foreach(var e in elems)
            {
                AddElement(e);
            }
        }
        
        public Dictionary<String, TimelineElement>.ValueCollection Children
        {
            get
            {
                return elements.Values;
            }
        }

        public Dictionary<String, TimelineElement>.KeyCollection ChildrenKeys
        {
            get
            {
                return elements.Keys;
            }
        }

        private void UpdateElementValues()
        {
            startOffset = decimal.MinValue;
            endPos = decimal.MinValue;
            stepSize = decimal.MinValue;
            foreach (var e in elements.Values)
            {
                if (decimal.MinValue == startOffset)
                {
                    startOffset = e.Offset;
                    endPos = e.EndPosition;
                    stepSize = e.StepSize;
                }
                else
                {
                    startOffset = Math.Max(startOffset, e.Offset);
                    endPos = Math.Max(endPos, e.EndPosition);
                    stepSize = Math.Max(stepSize, e.StepSize);
                }
            }
        }

        public void ClearAllElements()
        {
            Pause();
            AdvanceCurrentPosition(-CurrentPosition);
            foreach(var id in elements.Keys)
            {
                TimelineElement elem = elements[id];
                elem.OffsetChanged -= elem_OffsetChanged;
                elem.Dispose();               
            }
            elements.Clear();
            UpdateElementValues();
        }

        public void RemoveElement(TimelineElement element)
        {
            RemoveElement(element.GetUniqueID());
        }

        public void RemoveElement(string id)
        {
            if (elements.ContainsKey(id))
            {
                TimelineElement elem = elements[id];
                elem.OffsetChanged -= elem_OffsetChanged;
                elements.Remove(id);
                elem.Dispose();
                UpdateElementValues();
            }
        }

        public void elem_OffsetChanged(object sender, EventArgs e)
        {
            UpdateElementValues();
        }

        public void Play()
        {
            if (paused)
            {
                paused = false;
                stp.Start();
                foreach (var e in elements.Values)
                {
                    e.Play();
                }
            }
        }

        public void Pause()
        {
            if (!paused)
            {
                paused = true;
                stp.Stop();
                foreach (var e in elements.Values)
                {
                    e.Pause();
                }
                ResyncElements();
            }
        }

        public void Update()
        {
            decimal time = CurrentPosition;
            foreach (var e in elements.Values)
            {
                e.Update(time + e.Offset);
            }
        }

        public decimal EndPosition
        {
            get
            {
                return endPos;
            }
        }

        public decimal CurrentPosition
        {
            get
            {
                if (startOffset == Decimal.MinValue)
                {
                    return 0m;
                }
                decimal result = startOffset + currentRelPosition + (((decimal)stp.Elapsed.TotalSeconds) * playSpeed);
                if(result <= EndPosition)
                {
                    return Math.Max(0,result);
                } else {
                    return Math.Max(0,EndPosition);
                }
            }
        }

        public void ResyncElements()
        {
            foreach (var e in elements.Values)
            {
                e.CurrentPosition = CurrentPosition + e.Offset;
            }
        }

        public void AdvanceCurrentPosition(decimal seconds)
        {
            bool p = paused;
            Pause();
            if (CurrentPosition + seconds <= 0)
            {
                if (startOffset == Decimal.MinValue)
                {
                    currentRelPosition = -(((decimal)stp.Elapsed.TotalSeconds) * playSpeed);
                }
                else
                {
                    currentRelPosition = -startOffset - (((decimal)stp.Elapsed.TotalSeconds) * playSpeed);
                }
                ResyncElements();

                //currentRelPosition += seconds;
                //throw new ArgumentOutOfRangeException("Timeline.AdvanceCurrentPosition", seconds, "Cannot move past timecode 0 in the timeline.");
            }
            else
            {
                currentRelPosition += seconds;
                ResyncElements();
            }

            if (!p)
            {
                Play();
            }
        }

        public int Count
        {
            get
            {
                return elements.Count;
            }
        }

        public decimal StepSize
        {
            get
            {
                return stepSize;
            }
        }

        /// <summary>
        /// Gets or sets the play speed.
        /// </summary>
        /// <value>
        /// The play speed. Must be positive and less than infinity. NaN is not allowed.
        /// </value>
        public decimal PlaySpeed
        {
            get
            {
                return playSpeed;
            }
            set
            {
                if(value <= 0)
                {
                    throw new ArgumentOutOfRangeException("Timeline.PlaySpeed");
                }
                if (playSpeed != value)
                {
                    currentRelPosition += (decimal)(stp.Elapsed.TotalSeconds) * playSpeed;
                    if(!paused)
                    {
                        stp.Restart();
                    }
                    else
                    {
                        stp.Reset();
                    }
                    playSpeed = value;
                    foreach (var e in elements.Values)
                    {
                        e.PlaySpeed = playSpeed;
                    }
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="Timeline"/> is paused.
        /// </summary>
        /// <value>
        ///   <c>true</c> if paused; otherwise, <c>false</c>.
        /// </value>
        public bool Paused
        {
            get
            {
                return paused;
            }
        }

        public override string ToString()
        {
            int rate = (int)Math.Round(playSpeed * 10);
            return Utils.CreateTimeString(CurrentPosition).Append(" / ").Append(Utils.CreateSecondString(CurrentPosition)).AppendFormat(" @ {0}.{1}x", rate / 10, rate % 10).ToString();
        }
        
    }
}
