﻿using Annotator.Controllers;
using Annotator.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Annotator.TimelineElements
{

    public abstract class TimelineElementBase : ITimelineElement
    {
        protected readonly ElementType type;
        protected string filepath;

        protected const char SEPARATOR = '\t';
        public const decimal DEFAULT_OFFSET = 0.0m;
        protected decimal offset = DEFAULT_OFFSET;

        protected TimelineElementBase(ElementType type, string filepath)
        {
            this.type = type;
            this.filepath = filepath;
        }

        public virtual decimal Offset
        {
            get
            {
                return offset;
            }
            set
            {
                offset = value;
            }
        }

        public ElementType Type
        {
            get
            {
                return type;
            }
        }

        public string Filepath
        {
            get
            {
                return filepath;
            }
        }

        public abstract TimelineElementBuilder CreateBuilder();

        public override abstract string ToString();

    }

    public abstract class TimelineElementBuilder : TimelineElementBase
    {
        protected TimelineElementBuilder(ElementType type, string filepath) : base(type, filepath)
        {

        }

        public void ChangeFilepath(string newpath)
        {
            filepath = newpath;
        }

        public abstract void BuildAndAdd(MainController controller);

        public static TimelineElementBuilder CreateFromString(string s)
        {
            String[] values = s.Split(SEPARATOR);
            int t;
            if (!int.TryParse(values[0], out t))
            {
                //for backwards compatibility
                return VideoElement.VideoElementBuilder.ParseString(new string[] { null, values[0], values[1] });
            }
            else
            {
                ElementType type = (ElementType)t;
                switch (type)
                {
                    case ElementType.VIDEO:
                        return VideoElement.VideoElementBuilder.ParseString(values);
                    case ElementType.CHART:
                        return ChartElement.ChartElementBuilder.ParseString(values);
                    case ElementType.AUDIO:
                        return AudioElement.AudioElementBuilder.ParseString(values);
                    default:
                        throw new ArgumentException("Type " + values[0] + " not recognized.");
                }
            }
        }

        public override TimelineElementBuilder CreateBuilder()
        {
            return this;
        }

        public override string ToString()
        {
            return string.Format("TimelineElementBuilder [type={0}, filepath={1}]", type.ToString(), filepath);
        }
    }

    public abstract class TimelineElement : TimelineElementBase, IDisposable
    {
        public const decimal DEFAULT_PLAY_SPEED = 1.0m;
        public const bool DEFAULT_PAUSED = true;

        protected decimal playSpeed = DEFAULT_PLAY_SPEED;
        private bool paused = DEFAULT_PAUSED;

        public event EventHandler OffsetChanged;

        protected TimelineElement(ElementType type, string filepath)
            : base(type, filepath)
        {
        }

        /// <summary>
        /// Gets or sets the time offset in the Timeline.
        /// Calls OnOffsetChanged if the value changes.
        /// </summary>
        /// <value>
        /// The offset in seconds. Must not be infinity or NaN.
        /// </value>
        public override decimal Offset
        {
            get
            {
                return offset;
            }
            set
            {
                //if(Double.IsInfinity(value) || Double.IsNaN(value))
                if (decimal.MinValue == value)
                {
                    throw new ArgumentOutOfRangeException("TimelineElement.Offset");
                }
                if (offset != value)
                {
                    decimal dOffset = value - offset;
                    offset = value;
                    OnOffsetChanged(dOffset);
                }
            }
        }

        public abstract decimal StepSize
        {
            get;
        }

        /// <summary>
        /// Gets or sets the play speed. Calls OnPlaySpeedChanged if the value changes.
        /// </summary>
        /// <value>
        /// The play speed. Must be positive and less than infinity. NaN is not allowed.
        /// </value>
        public decimal PlaySpeed
        {
            get
            {
                return playSpeed;
            }
            set
            {
                //if(Double.IsInfinity(value) || Double.IsNaN(value) || value <= 0)
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException("TimelineElement.PlaySpeed");
                }
                if (playSpeed != value)
                {
                    playSpeed = value;
                    OnPlaySpeedChanged();
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="TimelineElement"/> is paused.
        /// </summary>
        /// <value>
        ///   <c>true</c> if paused; otherwise, <c>false</c>.
        /// </value>
        public bool Paused
        {
            get
            {
                return paused;
            }
        }

        public abstract decimal CurrentPosition
        {
            get;
            set;
        }

        public abstract decimal EndPosition
        {
            get;
        }

        protected abstract void OnPlaySpeedChanged();

        protected virtual void OnOffsetChanged(decimal dOffset)
        {
            EventHandler handler = OffsetChanged;
            if (handler != null)
            {
                handler.Invoke(this, EventArgs.Empty);
            }
        }

        public abstract String GetUniqueID();

        /// <summary>
        /// Plays this <see cref="TimelineElement"/>.
        /// </summary>
        public virtual void Play()
        {
            paused = true;
        }

        /// <summary>
        /// Pauses this <see cref="TimelineElement"/>.
        /// </summary>
        public virtual void Pause()
        {
            paused = false;
        }

        public abstract void Update(decimal currentTime);

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // NOTE: Leave out the finalizer altogether if this class doesn't 
        // own unmanaged resources itself, but leave the other methods
        // exactly as they are. 
        ~TimelineElement() 
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                OffsetChanged = null;
            }
        }

        #endregion
    }
}
