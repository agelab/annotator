﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Annotator.Controls
{
    public abstract class MediaInfoPanel : UserControl
    {
        public abstract void AddKeyDownHandler(KeyEventHandler handler);

        public abstract void RemoveKeyDownHandler(KeyEventHandler handler);
    }

    public partial class VideoInfoPanel : MediaInfoPanel
    {
        public VideoInfoPanel()
        {
            InitializeComponent();
        }

        public delegate void OffsetChangedEventHandler<Decimal>(object sender, ValueEventArgs<decimal> e);
        public event OffsetChangedEventHandler<decimal> OffsetChanged;

        public event EventHandler Removed;

        public void SetNewVideoName(string path, string name)
        {
            VideoName = name;
            VideoPath = path;
        }

        public String VideoPath
        {
            get;
            private set;
        }

        public String VideoName
        {
            get
            {
                return nameLabel.Text;
            }
            private set
            {
                nameLabel.Text = value;
            }
        }

        public String VideoLength
        {
            get
            {
                return lengthLabel.Text;
            }
            set
            {
                lengthLabel.Text = value;
            }
        }

        public decimal Offset
        {
            get
            {
                return numericUpDown1.Value;
            }
            set
            {
                numericUpDown1.Value = value;
            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            OffsetChangedEventHandler<decimal> handler = OffsetChanged;
            if (handler != null)
            {
                handler(this, new ValueEventArgs<decimal>(numericUpDown1.Value));
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            EventHandler handler = Removed;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        public override void AddKeyDownHandler(KeyEventHandler handler)
        {
            numericUpDown1.KeyDown += handler;
            closeButton.KeyDown += handler;
        }

        public override void RemoveKeyDownHandler(KeyEventHandler handler)
        {
            numericUpDown1.KeyDown -= handler;
            closeButton.KeyDown -= handler;
        }

    }

    public class ValueEventArgs<T> : EventArgs
    {
        public ValueEventArgs(T value)
        {
            Value = value;
        }

        /*public Boolean Success
        {
            get;
            set;
        }*/

        public T Value
        {
            get;
            private set;
        }
    }

}
