﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Annotator.Controls
{
    public class DescriptionTextBox : TextBox
    {
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                FindForm().Focus();
            }
            e.Handled = true;
            base.OnKeyDown(e);
        }
    }
}
