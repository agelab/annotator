﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Annotator.Controls
{
    public class ToolStripNumericTextBox : ToolStripTextBox
    {
        private readonly Color emptyColor = Color.White;
        private readonly Color validColor = Color.LightGreen;
        private readonly Color invalidColor = Color.LightPink;

        protected override void OnTextChanged(EventArgs e)
        {
            decimal value;
            HasNumericValue = decimal.TryParse(Text, out value);
            if (Text.Trim().Length == 0)
            {
                HasNumericValue = false;
                BackColor = emptyColor;
            }
            else if (HasNumericValue)
            {
                NumericValue = value;
                BackColor = validColor;
            }
            else
            {
                BackColor = invalidColor;
            }
            base.OnTextChanged(e);
        }

        public bool HasNumericValue
        {
            get;
            private set;
        }

        public decimal NumericValue
        {
            get;
            private set;
        }

    }
}
