﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Annotator.Controls
{
    public partial class ChartInfoPanel : MediaInfoPanel
    {
        public ChartInfoPanel()
        {
            InitializeComponent();
        }

        public delegate void OffsetChangedEventHandler<Decimal>(object sender, ValueEventArgs<decimal> e);
        public event OffsetChangedEventHandler<decimal> OffsetChanged;

        public delegate void ColumnChangedEventHandler<String>(object sender, ValueEventArgs<string> e);

        public event EventHandler Removed;

        public void SetNewChartName(string path, string name)
        {
            ChartName = name;
            FilePath = path;
        }

        public String ChartName
        {
            get
            {
                return columnLabel.Text;
            }
            private set
            {
                columnLabel.Text = value;
            }
        }

        public String FilePath
        {
            get
            {
                return filenameLabel.Text;
            }
            private set
            {
                filenameLabel.Text = value;
            }
        }

        public decimal Offset
        {
            get
            {
                return numericUpDown1.Value;
            }
            set
            {
                numericUpDown1.Value = value;
            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            OffsetChangedEventHandler<decimal> handler = OffsetChanged;
            if (handler != null)
            {
                handler(this, new ValueEventArgs<decimal>(numericUpDown1.Value));
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            EventHandler handler = Removed;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        public override void AddKeyDownHandler(KeyEventHandler handler)
        {
            numericUpDown1.KeyDown += handler;
            closeButton.KeyDown += handler;
        }

        public override void RemoveKeyDownHandler(KeyEventHandler handler)
        {
            numericUpDown1.KeyDown -= handler;
            closeButton.KeyDown -= handler;
        }

    }

}
