﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Annotator.Controls
{
    public class ToolStripUpdateableComboBox : ToolStripComboBox
    {

        public void UpdatePossibleValues(IEnumerable<string> newValues)
        {
            string selection = (string)SelectedItem;
            this.Items.Clear();
            bool exists = false;
            foreach(string s in newValues)
            {
                if (s == selection)
                {
                    exists = true;
                }
                this.Items.Add(s);
            }
            if (exists)
            {
                this.SelectedItem = selection;
            }
            else
            {
                this.SelectedItem = null;
            }
        }

    }
}
