﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Annotator.Controls
{
    public class ShortcutTextBox : TextBox
    {
        private Keys shortcut = Keys.None;
        private Color oldBackcolor;
        public event ShortcutChangedEventHandler ShortcutChanged;

        public ShortcutTextBox()
        {
            BackColor = Color.LightGray;
            HighlightColor = Color.White;
            TabStop = false;
            oldBackcolor = BackColor;
            Text = shortcut.ToString();
        }

        protected override bool ShowFocusCues
        {
            get
            {
                return false;
            }
        }

        protected override bool ShowKeyboardCues
        {
            get
            {
                return false;
            }
        }

        public virtual Keys Shortcut
        {
            get
            {
                return shortcut;
            }
        }

        public virtual Color HighlightColor
        {
            get;
            set;
        }

        public virtual bool IsActive
        {
            get
            {
                return BackColor == HighlightColor;
            }
        }

        public virtual void SetShortcut(Keys k, bool userInput = false)
        {
            if (shortcut != k)
            {
                OnShortcutChanged(new ShortcutChangedEventArgs(k, shortcut, userInput));
            }            
        }
        
        protected virtual void OnShortcutChanged(ShortcutChangedEventArgs e)
        {
            if (ShortcutChanged != null)
            {
                ShortcutChanged(this, e);
            }
            if (e.Succeeded)
            {
                shortcut = e.Shortcut;
                this.Text = e.Shortcut.ToString();
            }
        }

        protected override void OnClick(EventArgs e)
        {
            if (IsActive)
            {
                LoseFocus();
            }
            else
            {
                oldBackcolor = BackColor;
                BackColor = HighlightColor;
            }
            base.OnClick(e);
        }

        protected override void OnLostFocus(EventArgs e)
        {
            BackColor = oldBackcolor;
            base.OnLostFocus(e);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (IsActive)
            {
                SetShortcut(e.KeyCode, true);
            }

            e.SuppressKeyPress = true;
            e.Handled = true;
            base.OnKeyDown(e);
        }

        protected override void OnKeyUp(KeyEventArgs e)
        {
            e.Handled = true;
            e.SuppressKeyPress = true;
            base.OnKeyUp(e);
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            e.Handled = true;
            base.OnKeyPress(e);
        }

        public void LoseFocus()
        {
            FindForm().Focus();
        }

    }

    public delegate void ShortcutChangedEventHandler(object sender, ShortcutChangedEventArgs e);

    public class ShortcutChangedEventArgs : EventArgs
    {
        private readonly Keys key;
        private readonly Keys oldKey;
        private readonly bool userInput;  //true if the user caused the shortcut to be changed, false otherwise (if loaded new shortcut file for example)
        
        public ShortcutChangedEventArgs(Keys key, Keys oldKey, bool userInput)
        {
            this.key = key;
            this.oldKey = oldKey;
            this.userInput = userInput;
            Succeeded = true;
        }

        public Keys OldShortcut
        {
            get
            {
                return oldKey;
            }
        }

        public Keys Shortcut
        {
            get
            {
                return key;
            }
        }

        public bool IsUserInput
        {
            get
            {
                return userInput;
            }
        }

        public bool Succeeded
        {
            get;
            set;
        }

    }
}
