﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Annotator.Controls
{
    public class ColorFeedbackTextBox : TextBox
    {
        private readonly Color emptyColor = Color.White;
        private readonly Color validColor = Color.LightGreen;
        private readonly Color invalidColor = Color.LightPink;

        public ColorFeedbackTextBox() : base()
        {
            this.BackColor = emptyColor;
        }

        public Func<string, bool> ValidityCondition
        {
            get;
            set;
        }

        protected override void OnTextChanged(EventArgs e)
        {
            RefreshValidity();
            base.OnTextChanged(e);
        }

        public void RefreshValidity()
        {
            string contents = Text.Trim();
            if (contents.Length == 0)
            {
                BackColor = emptyColor;
            }
            else
            {
                if (ValidityCondition == null || ValidityCondition(contents))
                {
                    BackColor = validColor;
                }
                else
                {
                    BackColor = invalidColor;
                }
            }
        }

    }
}
