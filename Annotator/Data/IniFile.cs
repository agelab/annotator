﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Annotator.Data
{
    /// <summary>
    /// Create a New INI file to store or load data
    /// 
    /// Based on original code published on CodeProject:
    /// http://www.codeproject.com/Articles/1966/An-INI-file-handling-class-using-C
    /// </summary>
    public class IniFile
    {
        public readonly string path;

        [DllImport("KERNEL32.DLL",   EntryPoint = "GetPrivateProfileStringW",
           SetLastError=true,
           CharSet=CharSet.Unicode, ExactSpelling=true,
           CallingConvention=CallingConvention.StdCall)]
         private static extern int GetPrivateProfileString(
           string lpAppName,
           string lpKeyName,
           string lpDefault,
           StringBuilder lpReturnString,
           int nSize,
           string lpFilename);
 
        [DllImport("KERNEL32.DLL", EntryPoint="WritePrivateProfileStringW",
           SetLastError=true,
           CharSet=CharSet.Unicode, ExactSpelling=true,
           CallingConvention=CallingConvention.StdCall)]
         private static extern int WritePrivateProfileString(
           string lpAppName,
           string lpKeyName,
           string lpString,
           string lpFilename);

        /// <summary>
        /// INIFile Constructor.
        /// </summary>
        /// <PARAM name="INIPath"></PARAM>
        public IniFile(string INIPath)
        {
            path = INIPath;
        }
        /// <summary>
        /// Write Data to the INI File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// Section name
        /// <PARAM name="Key"></PARAM>
        /// Key Name
        /// <PARAM name="Value"></PARAM>
        /// Value Name
        public void WriteValue(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, this.path);
        }

        /// <summary>
        /// Read Data Value From the Ini File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// <PARAM name="Key"></PARAM>
        /// <PARAM name="Path"></PARAM>
        /// <returns></returns>
        public string ReadValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp,
                                            255, this.path);
            return temp.ToString();

        }
    }
}
