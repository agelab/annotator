﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;

namespace Annotator.Data
{
    public class DataFile
    {
        private readonly string filepath;
        private readonly string[] columns;
        private readonly Dictionary<string, List<decimal>> data = new Dictionary<string,List<decimal>>();
        
        private List<decimal> normalizedTime = null;
    
        private decimal initialTime = NO_TIME;
        private int timingColumnIndex = NO_TIMING_COLUMN;

        private const decimal NO_TIME = decimal.MinValue;
        private const int NO_TIMING_COLUMN = -1;
        private const char SEPARATOR = '\t';
        private const bool DEFAULT_LAZY_OPTION = false;
        private const decimal UNITS_PER_SECOND = 10000000m;

        public DataFile(string filepath) : this(filepath, DEFAULT_LAZY_OPTION)
        {
            
        }

        public DataFile(string filepath, bool isLazy)
        {
            this.filepath = filepath;
            using (var sr = new StreamReader(filepath))
            {
                columns = sr.ReadLine().Split(SEPARATOR);
            }
            if (!isLazy)
            {
                LoadAllColumns();
            }
        }

        public string Filepath
        {
            get
            {
                return filepath;
            }
        }

        public string[] Columns
        {
            get
            {
                return columns;
            }
        }

        public List<decimal> TimingData
        {
            get
            {
                if (TimingColumn == null) return null;
                return data[TimingColumn];
            }
        }

        public string TimingColumn
        {
            get
            {
                if (timingColumnIndex == -1) return null;
                return columns[timingColumnIndex];
            }
            set
            {
                int index = GetColumnIndex(value);
                if (index == -1)
                {
                    throw new ArgumentException("TimingColumn name " + value + " was not found in the file.");
                }

                SetTimingColumn(value);
                normalizedTime = null;
                timingColumnIndex = index;
            }
        }

        public int TimingColumnIndex
        {
            get
            {
                return timingColumnIndex;
            }
            set
            {
                if (value != NO_TIMING_COLUMN && (value < 0 || value >= columns.Length))
                {
                    throw new ArgumentOutOfRangeException("TimingColumnIndex may not be negative or greater than the number of columns in the file unless it is equal to NO_TIMING_COLUMN.");
                }

                SetTimingColumn(columns[value]);
                timingColumnIndex = value;                
            }
        }

        private bool CheckLinearTime(List<decimal> times)
        {
            if (times.Count == 0) return false;
            decimal t = times[0];
            int i = 1;

            while (i < times.Count)
            {
                if (times[i] <= t)
                {
                    return false;
                }
                t = times[i];
                i++;
            }
            return true;
        }

        private void InitializeTime(decimal newTime)
        {
            initialTime = newTime;
        }

        private void SetTimingColumn(string columnName)
        {
            if (CheckLinearTime(data[columnName]))
            {
                InitializeTime(data[columnName][0]);
                GetColumn(columnName);
            }
            else
            {
                throw new ArgumentException("In a time column, timestamps must increase the further down in the file they are.");
            }
        }

        private int GetColumnIndex(string columnName)
        {
            int i;
            for (i = 0; i < columns.Length; i++)
            {
                if (columns[i] == columnName)
                {
                    return i;
                }
            }
            return -1;
        }

        private void LoadAllColumns()
        {
            List<decimal>[] lists = new List<decimal>[columns.Length];
            decimal value;
            int i;
            DateTime date;
            String[] rowFields;
            bool valid;

            for (i = 0; i < columns.Length; i++)
            {
                lists[i] = new List<decimal>();
            }

            using (var sr = new StreamReader(filepath))
            {
                sr.ReadLine();
                while (!sr.EndOfStream)
                {
                    rowFields = sr.ReadLine().Split(SEPARATOR);
                    for (i = 0; i < columns.Length; i++)
                    {
                        valid = lists[i] != null && !string.IsNullOrWhiteSpace(rowFields[i]);
                        
                        if (valid && decimal.TryParse(rowFields[i], System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.GetCultureInfoByIetfLanguageTag("en-US"), out value))
                        {
                            lists[i].Add(value);  
                        }
                        else
                        {
                            if (valid && DateTime.TryParse(rowFields[i], System.Globalization.CultureInfo.GetCultureInfoByIetfLanguageTag("en-US"), System.Globalization.DateTimeStyles.AllowWhiteSpaces, out date))
                            {
                                value = (decimal)date.ToFileTimeUtc() / UNITS_PER_SECOND;
                                lists[i].Add(value);
                            }
                            else if (valid && DateTime.TryParse(rowFields[i], out date))
                            {
                                value = (decimal)date.ToFileTimeUtc() / UNITS_PER_SECOND;
                                lists[i].Add(value);
                            }
                            else
                            {
                                valid = false;
                            }
                        }

                        if(!valid)
                        {
                            if (lists[i] != null)
                            {
                                lists[i].Clear();
                            }
                            lists[i] = null;
                        }
                    }
                }
            }

            for (i = 0; i < columns.Length; i++)
            {
                if (lists[i] != null)
                {
                    data.Add(columns[i], lists[i]);
                }
            }
        }

        private List<decimal> LoadColumn(string columnName)
        {
            int index = GetColumnIndex(columnName);

            if (index == -1)
            {
                throw new ArgumentException("Column " + columnName + " does not exist in file " + filepath);
            }

            List<decimal> res = new List<decimal>();
            
            using (var sr = new StreamReader(filepath))
            {
                decimal value;
                String item;
                DateTime date;
                sr.ReadLine();
                while (!sr.EndOfStream)
                {
                    item = sr.ReadLine().Split(SEPARATOR)[index];
                    if (!decimal.TryParse(item, out value))
                    {
                        if (!DateTime.TryParse(item, out date))
                        {
                            throw new ArgumentException("Invalid value in column " + columnName + ": " + item);
                        }
                        value = (decimal)(date.ToFileTimeUtc()) / UNITS_PER_SECOND;
                    }
                    res.Add(value);
                }
            }
            return res;
        }

        public decimal GetTimeWhenRangeReached(string dataColumn, decimal loThreshold, decimal hiThreshold, decimal startTime = 0)
        {
            List<decimal> time = GetNormalizedTimeColumn();
            List<decimal> data = GetColumn(dataColumn);
            int startIndex = time.BinarySearch(startTime);
            if (startIndex < 0)
            {
                startIndex = ~startIndex;
            }
            else
            {
                startIndex++;
            }
            for (; startIndex < data.Count; startIndex++)
            {
                if (data[startIndex] <= hiThreshold && data[startIndex] >= loThreshold)
                {
                    return time[startIndex];
                }
            }
            return Decimal.MinusOne;
        }

        public List<decimal> GetNormalizedTimeColumn()
        {
            if (normalizedTime == null)
            {
                List<decimal> origData = GetColumn(TimingColumn);
                normalizedTime = new List<decimal>();
                decimal factor = origData[0];
                for (int i = 0; i < origData.Count; i++)
                {
                    normalizedTime.Add(origData[i] - factor);
                }
            }
            return normalizedTime;
        }

        public List<decimal> GetColumn(string columnName)
        {
            if (data.ContainsKey(columnName))
            {
                return data[columnName];
            } else {
                List<decimal> res = LoadColumn(columnName);
                data.Add(columnName, res);
                return res;
            }
        }

        public AnnotationRowExtractor CreateExtractor()
        {
            return new AnnotationRowExtractor(this);
        }

        public class AnnotationRowExtractor : IEnumerable<String>
        {
            private readonly DataFile file;

            //TODO make multi-set to allow extraction of same row multiple times
            private readonly List<decimal> timesToExtract = new List<decimal>(); //extract rows with this timing info
            private readonly SortedSet<decimal> timings;
            private readonly List<int> columnsToSave;

            private static readonly String headerRowStub = String.Format("KeyPress{0}Description{0}AbsoluteTime", SEPARATOR);

            public AnnotationRowExtractor(DataFile file)
            {
                this.file = file;
                //assumes file is not modified between creation of extractor and output of annotations.
                if (file == null)
                {
                    throw new ArgumentNullException("AnnotationRowExtractor may not be created for null file.");
                }
                if (file.TimingData == null)
                {
                    throw new ArgumentNullException("Timing column must be set for data file " + file.Filepath);
                }

                timings = new SortedSet<decimal>(file.TimingData);
                columnsToSave = new List<int>();
            }

            //Extracts row with time t such that t+initialTime<=time and t is maximal.
            //time must be >= 0
            public void AnnotateTime(decimal time)
            {
                decimal endTime = file.initialTime + time;
                decimal added = timings.GetViewBetween(file.initialTime, endTime).Max;
                timesToExtract.Add(added);
            }

            public void AddColumn(string columnName)
            {
                int index = file.GetColumnIndex(columnName);
                if (index == -1)
                {
                    throw new ArgumentException("Column " + columnName + " was not found in file " + file.filepath);
                }
                columnsToSave.Add(index);
            }

            public void RemoveColumn(string columnName)
            {
                int index = file.GetColumnIndex(columnName);
                if (index == -1)
                {
                    throw new ArgumentException("Column " + columnName + " was not found in file " + file.filepath);
                }
                columnsToSave.Remove(index);
            }

            /*
             * Format:
             * KeyPress - Description - AbsoluteTime - DataColumn1 - DataColumn2 - ...
             */
            public String GetHeaderRow(string headerRowStub)
            {
                StringBuilder sb = new StringBuilder(headerRowStub);
                foreach (int column in columnsToSave)
                {
                    sb.Append(SEPARATOR);
                    sb.Append(file.Columns[column]);
                }
                string res = sb.ToString();
                sb.Clear();
                return res;
            }

            public IEnumerator<string> GetEnumerator()
            {
                int currentPos = 0;
                String[] data;
                bool nonEmpty;
                StringBuilder sb = new StringBuilder();
                String res;
                timesToExtract.Sort();
                using (var sr = new StreamReader(file.filepath))
                {
                    sr.ReadLine(); //read header row
                    foreach (decimal d in timesToExtract)
                    {
                        while(file.TimingData[currentPos] != d)
                        {
                            sr.ReadLine();
                            currentPos++;
                        }

                        data = sr.ReadLine().Split(SEPARATOR);
                        nonEmpty = false;
                        foreach (int column in columnsToSave)
                        {
                            if (nonEmpty)
                            {
                                sb.Append(SEPARATOR);
                            }
                            sb.Append(data[column]);
                            nonEmpty = true;
                        }
                        res = sb.ToString();
                        sb.Clear();
                        yield return res;
                    }
                }
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }

    }
}
