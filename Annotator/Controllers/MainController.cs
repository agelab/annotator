﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using Annotator.Annotations;
using Annotator.Controls;
using Annotator.Data;
using Annotator.Forms;
using Annotator.TimelineElements;
using Annotator.Utility;
using System.Collections.ObjectModel;
using Annotator.Plugins;
using OxyPlot.WindowsForms;

namespace Annotator.Controllers
{
    public class MainController
    {
        public const int DEFAULT_VIDEOPANEL_WIDTH = 320;
        public const int DEFAULT_VIDEOPANEL_HEIGHT = 240;
        public const int DEFAULT_CHARTPANEL_WIDTH = 480;
        public const int DEFAULT_CHARTPANEL_HEIGHT = 320;
        public const String REMOVE_DATA_SOURCE_PROMPT = "Are you sure you want to remove this data source?";

        private StringBuilder sb = new StringBuilder();
        private readonly MainForm mainForm;
        private readonly ChartForm chartForm;
        private readonly Timeline timeline = new Timeline();
        private readonly Annotator.Annotations.Annotator annotator = new Annotator.Annotations.Annotator();
        private readonly ShortcutController shortcutCtrl;
        private readonly PluginController pluginController;
        private readonly DataController dataController;
        private readonly List<ShortcutTextBox> shortcutBoxes;
        private bool canSeek = false;

        private const String VIDEO_HEIGHT_CATEGORY = "Video";
        private const String VIDEO_HEIGHT_PROPERTY = "video.height";
        private int videoHeight = DEFAULT_VIDEOPANEL_HEIGHT;

        private readonly HashSet<string> DATA_EXTENSIONS = new HashSet<string>() { ".txt", ".dat", ".tsv" };
        private readonly HashSet<string> VIDEO_EXTENSIONS = new HashSet<string>() { ".avi", ".mpeg", ".flv", ".mov", ".mp4", ".divx", ".xvid", ".mpg", ".mkv", ".wmv" };
        private readonly HashSet<string> AUDIO_EXTENSIONS = new HashSet<string>() { ".wav", ".mp3", ".mpa", ".ogg", ".wma" };

        public MainController(MainForm mainForm)
        {
            this.mainForm = mainForm;
            mainForm.FormClosed += (x, y) => pluginController.OnApplicationClosed();

            shortcutBoxes = new List<ShortcutTextBox>();
            dataController = new DataController(mainForm, this);
            chartForm = new ChartForm(this);
            chartForm.Show();

            Utils.ApplyToControlAndChildren(mainForm, x =>
                                                {
                                                    Type t = x.GetType();
                                                    if (t != typeof(ShortcutTextBox))
                                                    {
                                                        x.Click += ClickEventHandler;
                                                    }
                                                    else
                                                    {
                                                        shortcutBoxes.Add(x as ShortcutTextBox);
                                                    }
                                                    if (t != typeof(DescriptionTextBox) && t != typeof(ShortcutTextBox))
                                                    {
                                                        x.KeyDown += KeyDownEventHandler;
                                                        x.KeyUp += KeyUpEventHandler;
                                                    }
                                                    x.AllowDrop = true;
                                                    x.DragEnter += DragEnterEventHandler;
                                                    x.DragDrop += DragDropEventHandler;

                                                    //AddSessionLogData(x.GetType().Name);
                                                });
            /*
             * uncomment to enable adding F1-F12 as default annotations
            annotator.AddDefaultAnnotationKeys();
            foreach (var k in annotator.ReservedKeys)
            {
                mainForm.AddAnnotation(k, annotator.GetAnnotationKey(k).Description,false);
            }
            */

            shortcutCtrl = ShortcutController.CreateDefaultInstance(this);
            shortcutCtrl.RegisterFixedShortcut(Keys.Control | Keys.Z, () => { RemoveLastAnnotation(); });
            shortcutCtrl.RegisterFixedShortcut(Keys.Control | Keys.N, () => { mainForm.SkipToNextValueInRange(); });
            mainForm.ApplyTextToShortcutButtons(shortcutCtrl);

            pluginController = new PluginController(this);
            pluginController.Initialize(mainForm.pluginsToolStripMenuItem);
        }

        public void InitializeDataBindings()
        {
            mainForm.DataForm.InitializeDataBindings(annotator.BindingSource, annotator.IsValidSessionKey);
        }

        internal DataController DataControllerInstance
        {
            get
            {
                return dataController;
            }
        }

        public void LoadPropertiesFromFile(String path)
        {
            IniFile file = new IniFile(path);
            List<String> errorMessages = new List<string>();

            if (ApplyProperties(file, errorMessages) && 
                shortcutCtrl.ApplyProperties(file, this, errorMessages) && 
                annotator.ApplyProperties(file, this, errorMessages) &&
                pluginController.ApplyProperties(file, errorMessages))
            {
                mainForm.ApplyTextToShortcutButtons(shortcutCtrl);
                AddSessionLogData("Applied properties from ini file " + path);
            }
            else
            {
                StringBuilder sb = new StringBuilder("Could not apply some properties from ini file ").Append(path).AppendLine(" due to the following errors:");
                foreach (String s in errorMessages)
                {
                    sb.AppendLine(s);
                }
                AddSessionLogData(sb.ToString());
            }
        }

        public void SavePropertiesToFile(String path)
        {
            try
            {
                IniFile file = new IniFile(path);
                SaveProperties(file);
                shortcutCtrl.SaveProperties(file);
                annotator.SaveProperties(file);
                pluginController.SaveProperties(file);
                AddSessionLogData("Properties saved to file " + path);
            }
            catch (Exception ex)
            {
                AddSessionLogData("Properties could not be saved to file " + path + Environment.NewLine + "Reason: " + ex.ToString());
            }
        }

        public bool ApplyProperties(IniFile file, List<String> errorMessages)
        {
            int value;
            if (int.TryParse(file.ReadValue(VIDEO_HEIGHT_CATEGORY, VIDEO_HEIGHT_PROPERTY), out value) && value > 0)
            {
                videoHeight = value;
                return true;
            }
            return false;
        }

        public void SaveProperties(IniFile file)
        {
            file.WriteValue(VIDEO_HEIGHT_CATEGORY, VIDEO_HEIGHT_PROPERTY, videoHeight.ToString());
        }

        #region Playback handlers

        public void PushToPlayReleased()
        {
            if (timeline.Count == 0 || timeline.Paused)
            {
                return;
            }

            timeline.ResyncElements();
            timeline.Pause();
            mainForm.MakePlayButton();
            mainForm.StartStopTimer();
        }

        public void PushToPlay(object sender, EventArgs e)
        {
            if (timeline.Count == 0 || !timeline.Paused)
            {
                return;
            }
            timeline.ResyncElements();
            timeline.Play();
            mainForm.MakePauseButton();
            mainForm.StartStopTimer();
        }

        public void PlayPause(object sender, EventArgs e)
        {
            if (timeline.Count == 0)
            {
                return;
            }
            timeline.ResyncElements();
            if (timeline.Paused)
            {
                timeline.Play();
                mainForm.MakePauseButton();
            }
            else
            {
                timeline.Pause();
                mainForm.MakePlayButton();
            }
            mainForm.StartStopTimer();
        }

        public void IncreaseRate(object sender, EventArgs e)
        {
            if (timeline.Count == 0)
            {
                return;
            }

            mainForm.StartStopTimer();
            bool paused = timeline.Paused;
            timeline.Pause();

            timeline.PlaySpeed = timeline.PlaySpeed + Timeline.DEFAULT_RATE_STEP;
            Update();

            if (!paused)
            {
                timeline.Play();
            }
            mainForm.StartStopTimer();
        }

        public void DecreaseRate(object sender, EventArgs e)
        {
            if (timeline.Count == 0)
            {
                return;
            }

            mainForm.StartStopTimer();
            bool paused = timeline.Paused;
            timeline.Pause();
            try
            {
                timeline.PlaySpeed = timeline.PlaySpeed - Timeline.DEFAULT_RATE_STEP;
            }
            catch (ArgumentOutOfRangeException)
            {
                AddSessionLogData("Cannot decrease speed to a non-positive value.");
            }
            Update();

            if (!paused)
            {
                timeline.Play();
            }
            mainForm.StartStopTimer();
        }

        public void StepForward(object sender, EventArgs e)
        {
            if (timeline.Count == 0)
            {
                return;
            }

            mainForm.StartStopTimer();
            bool paused = timeline.Paused;
            timeline.Pause();

            timeline.AdvanceCurrentPosition(timeline.StepSize);
            Update();

            if (!paused)
            {
                timeline.Play();
            }
            mainForm.StartStopTimer();
        }

        public void StepBackward(object sender, EventArgs e)
        {
            if (timeline.Count == 0)
            {
                return;
            }

            mainForm.StartStopTimer();
            bool paused = timeline.Paused;
            timeline.Pause();
            try
            {
                timeline.AdvanceCurrentPosition(-timeline.StepSize);
            }
            catch (ArgumentOutOfRangeException)
            {
                AddSessionLogData("Cannot move past position 0.");
            }

            Update();

            if (!paused)
            {
                timeline.Play();
            }
            mainForm.StartStopTimer();
        }

        public void JumpForward(object sender, EventArgs e)
        {
            if (timeline.Count == 0)
            {
                return;
            }

            mainForm.StartStopTimer();
            bool paused = timeline.Paused;
            timeline.Pause();

            timeline.AdvanceCurrentPosition(Timeline.DEFAULT_JUMP_LENGTH);
            Update();

            if (!paused)
            {
                timeline.Play();
            }
            mainForm.StartStopTimer();
        }

        public void JumpBackward(object sender, EventArgs e)
        {
            if (timeline.Count == 0)
            {
                return;
            }

            mainForm.StartStopTimer();
            bool paused = timeline.Paused;
            timeline.Pause();

            try
            {
                timeline.AdvanceCurrentPosition(-Timeline.DEFAULT_JUMP_LENGTH);
            }
            catch (ArgumentOutOfRangeException)
            {
                AddSessionLogData("Cannot move past timecode 0.");
            }
            Update();

            if (!paused)
            {
                timeline.Play();
            }
            mainForm.StartStopTimer();
        }

        public void ValidateSeek(object sender, MouseEventArgs e)
        {
            canSeek = true;
        }

        public void InvalidateSeek(object sender, MouseEventArgs e)
        {
            canSeek = false;
        }

        public void Seek(decimal time)
        {
            if (timeline.Count == 0)
            {
                return;
            }
            mainForm.StartStopTimer();
            bool paused = timeline.Paused;
            timeline.Pause();
            try
            {
                timeline.AdvanceCurrentPosition(-timeline.CurrentPosition + time);
            }
            catch (ArgumentOutOfRangeException)
            {
                AddSessionLogData("Cannot move to desired position.");
            }
            Update();

            if (!paused)
            {
                timeline.Play();
            }
            mainForm.StartStopTimer();
        }

        public void SeekHandler(object sender, EventArgs e)
        {
            if (canSeek)
            {
                Seek(mainForm.GetTrackBarValue());
            }
        }

        public void Update()
        {
            mainForm.SetPlayLabelText(timeline.ToString());
            decimal position = timeline.CurrentPosition;
            mainForm.SetTrackBarValue((int)(position + 0.5m));
            timeline.Update();
            if (position == timeline.EndPosition && !timeline.Paused) {
                timeline.Pause();
                mainForm.StartStopTimer();
                mainForm.MakePlayButton();
            }
        }

        #endregion

        #region Drag-Drop handlers

        public void DragEnterEventHandler(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private bool HandleChartDrop(string filepath)
        {
            string dataColumn = null;
            DataFile df;
            try
            {
                df = new DataFile(filepath);
            }
            catch (ArgumentException ex)
            {
                String message = "The data file is improperly formatted and cannot be loaded. The most common cause of this error is duplicate column names.";
                throw new ArgumentException(message, ex);
            }
            using (ValueSelectorBox box = new ValueSelectorBox("Select data column", "Please select a data column to plot.", df.Columns))
            {
                if (box.ShowDialog() != DialogResult.OK)
                {
                    return false;
                }
                dataColumn = box.SelectedValue;
            }

            using (ValueSelectorBox box = new ValueSelectorBox("Select timing column", "Please select a timing column to use in the plot.", df.Columns))
            {
                if (box.ShowDialog() != DialogResult.OK)
                {
                    return false;
                }
                df.TimingColumn = box.SelectedValue;
            }
            AddChart(df, dataColumn, 0m);
            return true;
        }

        public void DragDropEventHandler(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                bool success = false;
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                //string t = "";
                foreach (var f in files)
                {
                    FileInfo info = new FileInfo(f);
                    string extension = info.Extension.ToLowerInvariant();
                    if (DATA_EXTENSIONS.Contains(extension))
                    {
                        try
                        {
                            success = HandleChartDrop(f);
                            AddSessionLogData(string.Format("Chart from file added: {0}", f));
                        }
                        catch (Exception ex)
                        {
                            AddSessionLogData(string.Format("Could not add chart from file {0} - {1}", f, ex.Message));
                        }
                    }
                    else if (VIDEO_EXTENSIONS.Contains(extension))
                    {
                        try
                        {
                            AddVideo(f, 0m);
                            AddSessionLogData(string.Format("Video file added: {0}", f));
                            success = true;
                        }
                        catch (Exception ex)
                        {
                            AddSessionLogData(string.Format("Could not add video file because of the following error: {0}",ex.Message));
                        }
                    }
                    else if (AUDIO_EXTENSIONS.Contains(extension))
                    {
                        try
                        {
                            AddAudio(f, 0m);
                            AddSessionLogData(string.Format("Audio file added: {0}", f));
                            success = true;
                        }
                        catch (Exception ex)
                        {
                            AddSessionLogData(string.Format("Could not add audio file because of the following error: {0}", ex.Message));
                        }
                    }
                    else
                    {
                        AddSessionLogData(string.Format("Unknown file type: {0}", extension));
                    }
                }
                if (success)
                {
                    mainForm.SetTrackBarBoundValues(0, (int)(timeline.EndPosition + 0.5m));
                }
                //mainForm.RedrawVideos();
                //mainForm.SetSessionLogLabelText(t);
                //timeline.Play();
            }
        }

        #endregion

        public void ClickEventHandler(object sender, EventArgs e)
        {
            if(!Object.ReferenceEquals(sender, mainForm.sessionLogTextBox))
            {
                mainForm.DefocusShortcutButtons();
            }
        }

        public void KeyDownEventHandler(object sender, KeyEventArgs e)
        {
            decimal position = Utils.RoundToTwoDecimals(timeline.CurrentPosition);

            bool active = false;
            foreach (ShortcutTextBox s in shortcutBoxes)
            {
                active = active | s.IsActive;
            }

            if (!active && !mainForm.IsDescriptionBoxFocused())
            {
                if (shortcutCtrl.ProcessKeyPress(sender, e, e.KeyData))
                {
                    e.Handled = true;
                }
                else if (annotator.AnnotateKeyPress(e.KeyData, position))
                {
                    mainForm.AddAnnotation(annotator.LastAddedAnnotation);
                    AddSessionLogData(String.Format("{0} pressed at timeline position {1}.", e.KeyData, position));
                    e.Handled = true;
                }
            }
        }

        public void KeyUpEventHandler(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == shortcutCtrl.GetKeyForAction(ShortcutKeyType.PushToPlay))
            {
                PushToPlayReleased();
                e.Handled = true;
            }
        }

        public void ShortcutChangedEventHandler(object sender, ShortcutChangedEventArgs e)
        {
            if (e.IsUserInput)
            {
                ShortcutKeyType type = (ShortcutKeyType)((ShortcutTextBox)sender).Tag;

                e.Succeeded = shortcutCtrl.ChangeShortcut(type, e.Shortcut, this);
            }
        }

        public void SaveAnnotations(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do you want to use your annotations to extract data from an external data set as well?", "Use data set", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            if (result == DialogResult.No)
            {
                using (SaveFileDialog sfd = new SaveFileDialog()
                {
                    AddExtension = true,
                    CheckPathExists = true,
                    DefaultExt = ".tsv",
                    DereferenceLinks = true,
                    Filter = "Tab-separated value files (*.tsv)|*.tsv|All files|*.*",
                    FilterIndex = 0,
                    OverwritePrompt = true,
                    SupportMultiDottedExtensions = true,
                    Title = "Save annotations..."
                })
                {
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        try
                        {
                            var data = annotator.MakeAnnotationFile(timeline);
                            pluginController.OnSavingAnnotations(data);
                            data = annotator.MakeAnnotationFile(timeline);
                            data.WriteToFile(sfd.FileName);
                            pluginController.OnSavedAnnotations(data, sfd.FileName);
                            AddSessionLogData("Annotations saved!");
                        }
                        catch (Exception ex)
                        {
                            AddSessionLogData("Error - could not save annotations." + Environment.NewLine + ex.ToString());
                            return;
                        }
                    }
                }
            }
            else if (result == DialogResult.Yes)
            {
                using (OpenFileDialog ofd = new OpenFileDialog()
                {
                    Multiselect = false,
                    CheckFileExists = true,
                    CheckPathExists = true,
                    DefaultExt = ".txt",
                    DereferenceLinks = true,
                    SupportMultiDottedExtensions = true,
                    Title = "Select file from which to draw annotation data..."
                })
                {
                    if (ofd.ShowDialog() != DialogResult.OK)
                    {
                        return;
                    }

                    try
                    {
                        DataFile source = new DataFile(ofd.FileName);  //TODO Make lazy loading enabled when tested

                        using (var selector = new ValueSelectorBox("Timing column", "Please choose a timing column according to which annotations will be extracted.", source.Columns))
                        {
                            bool retry = true;
                            while (retry)
                            {
                                if (selector.ShowDialog() != DialogResult.OK)
                                {
                                    return;
                                }
                                try
                                {
                                    source.TimingColumn = selector.SelectedValue;
                                    retry = false;
                                }
                                catch (ArgumentException ex)
                                {
                                    MessageBox.Show("Unable to use selected column for timing:" + Environment.NewLine + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return;
                                }
                            }
                        }

                        var extractor = source.CreateExtractor();

                        using (var columnSelector = new MultiValueSelectorBox("Column selection", "Please choose the columns that should be extracted with the annotations.", source.Columns))
                        {
                            if (columnSelector.ShowDialog() != DialogResult.OK)
                            {
                                return;
                            }

                            foreach (var c in source.Columns)
                            {
                                if (columnSelector.IsChecked(c))
                                {
                                    extractor.AddColumn(c);
                                }
                            }
                        }

                        using (SaveFileDialog sfd = new SaveFileDialog()
                        {
                            AddExtension = true,
                            CheckPathExists = true,
                            DefaultExt = ".tsv",
                            DereferenceLinks = true,
                            OverwritePrompt = true,
                            SupportMultiDottedExtensions = true,
                            Title = "Save annotations..."
                        })
                        {
                            if (sfd.ShowDialog() == DialogResult.OK)
                            {
                                try
                                {
                                    annotator.MakeAnnotationFile(timeline, extractor).WriteToFile(sfd.FileName);
                                    var data = annotator.MakeAnnotationFile(timeline, extractor);
                                    pluginController.OnSavingAnnotations(data);
                                    data = annotator.MakeAnnotationFile(timeline, extractor);
                                    data.WriteToFile(sfd.FileName);
                                    pluginController.OnSavedAnnotations(data, sfd.FileName);
                                    AddSessionLogData("Annotations saved!");
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("Error - could not save annotations." + Environment.NewLine + ex.ToString());
                                    return;
                                }
                            }
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MessageBox.Show("Unable to load file:" + Environment.NewLine + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }

        public void AddSessionLogData(string text)
        {
            sb.AppendLine(text);
            mainForm.SetSessionLogText(sb.ToString());
        }

        public void AddAnnotationToMainForm(Keys key, String description)
        {
            mainForm.AddAnnotationKey(key, description, false);
        }

        public void RemoveAnnotationKeyFromAnnotator(Keys key)
        {
            annotator.RemoveAnnotationKey(key);
        }

        public void AddAnnotationKeyToAnnotator(Keys key, String description)
        {
            annotator.AddAnnotationKey(key, new AnnotationKey(key, description));
        }

        public bool ChangeAnnotationShortcut(Keys oldKey, Keys key)
        {
            bool res;
            if ((res = (annotator.ReservedKeys.Contains(oldKey) && !annotator.ReservedKeys.Contains(key) && !shortcutCtrl.ReservedKeys.Contains(key))))
            {
                var ann = annotator.GetAnnotationKey(oldKey);
                annotator.RemoveAnnotationKey(oldKey);
                ann.Key = key;
                annotator.AddAnnotationKey(key, ann);
            }
            return res;
        }

        public bool ChangeAnnotationDescription(Keys key, String description)
        {
            bool res;
            if (res = annotator.ReservedKeys.Contains(key))
            {
                annotator.GetAnnotationKey(key).Description = description;
            }
            return res;
        }

        public Keys GetNextAnnotationKey()
        {
            Keys k = Keys.F1;
            int val = (int)k;
            while (annotator.ReservedKeys.Contains(k) || shortcutCtrl.ReservedKeys.Contains(k))
            {
                val++;
                k = (Keys)val;
            }
            return k;
        }

        public void VideoRemoved(object sender, EventArgs e)
        {
            if (MessageBox.Show(REMOVE_DATA_SOURCE_PROMPT, "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                VideoInfoPanel p = (VideoInfoPanel)sender;
                dataController.RemoveVideo(p.VideoPath);  //removes from main form
                mainForm.DataForm.RemoveMediaInfoPanel(p);
            }
        }

        public void AudioRemoved(object sender, EventArgs e)
        {
            if (MessageBox.Show(REMOVE_DATA_SOURCE_PROMPT, "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                VideoInfoPanel p = (VideoInfoPanel)sender;
                dataController.RemoveAudio(p.VideoPath);  //removes from main form
                mainForm.DataForm.RemoveMediaInfoPanel(p);
            }
        }

        public void VideoOffsetChanged(object sender, ValueEventArgs<decimal> e)
        {
            VideoInfoPanel p = (VideoInfoPanel)sender;
            //PlayPause(sender, EventArgs.Empty);
            bool isPaused = timeline.Paused;
            timeline.Pause();
            dataController.ChangeVideoOffset(p.VideoPath, p.Offset);
            timeline.ResyncElements();
            if (!isPaused)
            {
                timeline.Play();
            }
            //PlayPause(sender, EventArgs.Empty);
        }

        public void AudioOffsetChanged(object sender, ValueEventArgs<decimal> e)
        {
            VideoInfoPanel p = sender as VideoInfoPanel;
            bool isPaused = timeline.Paused;
            timeline.Pause();
            dataController.ChangeAudioOffset(p.VideoPath, p.Offset);
            timeline.ResyncElements();
            if (!isPaused)
            {
                timeline.Play();
            }
        }

        public void ChartRemoved(object sender, EventArgs e)
        {
            if (MessageBox.Show(REMOVE_DATA_SOURCE_PROMPT, "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                ChartInfoPanel p = (ChartInfoPanel)sender;
                dataController.RemoveChart(p.ChartName);  //removes from main form
                mainForm.DataForm.RemoveMediaInfoPanel(p);
            }
        }

        public void ChartOffsetChanged(object sender, ValueEventArgs<decimal> e)
        {
            ChartInfoPanel p = (ChartInfoPanel)sender;
            //PlayPause(sender, EventArgs.Empty);
            bool isPaused = timeline.Paused;
            timeline.Pause();
            dataController.ChangeChartOffset(p.ChartName, p.Offset);
            timeline.ResyncElements();
            if (!isPaused)
            {
                timeline.Play();
            }
            //PlayPause(sender, EventArgs.Empty);
        }

        public void FocusMainForm()
        {
            mainForm.Focus();
        }

        public Timeline Timeline
        {
            get
            {
                return timeline;
            }
        }

        public void ClearAnnotations()
        {
            annotator.ClearAnnotations();
            AddSessionLogData("All annotations cleared.");
        }

        public void RemoveLastAnnotation()
        {
            Annotation ann = annotator.LastAddedAnnotation;
            if (annotator.RemoveLastAddedAnnotation())
            {
                mainForm.RemoveAnnotation(ann);
                AddSessionLogData("The last annotation was removed.");
            }
            else
            {
                AddSessionLogData("Could not remove the last annotation.");
            }
        }

        public void AddChart(DataFile df, string dataColumn, decimal offset)
        {
            chartForm.AddChart(dataController.AddChart(df, dataColumn, DragEnterEventHandler, DragDropEventHandler, DEFAULT_CHARTPANEL_HEIGHT, DEFAULT_CHARTPANEL_WIDTH));
        }

        public void AddChart(string filepath, string timeColumn, string dataColumn, decimal offset)
        {
            DataFile df = new DataFile(filepath);
            df.TimingColumn = timeColumn;
            AddChart(df, dataColumn, offset);
        }

        public void RemoveChart(Plot chart)
        {
            chartForm.RemoveChart(chart);
        }

        public void AddVideo(string filepath, decimal offset)
        {
            VideoElement ve = dataController.AddVideo(filepath, DragEnterEventHandler, DragDropEventHandler, videoHeight, DEFAULT_VIDEOPANEL_WIDTH, videoHeight, DEFAULT_VIDEOPANEL_WIDTH);
            ve.Offset = offset;
            mainForm.AddVideoPanel(ve.DrawSurface);
        }

        public void RemoveVideo(Panel panel, string path)
        {
            mainForm.RemoveVideoPanel(panel);
            timeline.RemoveElement(path);
        }

        public void AddAudio(string filepath, decimal offset)
        {
            AudioElement ae = dataController.AddAudio(filepath);
            ae.Offset = offset;
        }

        public void RemoveAudio(string filepath)
        {
            timeline.RemoveElement(filepath);
        }

        public void ClearAllElements(bool isUserInput = true)
        {
            if (!timeline.Paused)
            {
                PlayPause(null, null);
            }
            foreach (Keys key in annotator.ReservedKeys)
            {
                mainForm.RemoveAnnotationKey(key, false);
            }
            annotator.ClearAnnotationKeys();
            annotator.ClearAnnotations();
            annotator.ClearSessionKeyValues();
            dataController.ClearAllElements();
            timeline.ClearAllElements();
            mainForm.ClearAnnotations();
            mainForm.RemoveAllVideoPanels();
            mainForm.DataForm.ClearAllElements();
            chartForm.RemoveAllCharts();
            mainForm.ScrollAnnotationButtonIntoView();
            Update();
            if (isUserInput)
            {
                AddSessionLogData("Workspace cleared!");
            }
        }

        public void LoadFromAnnotationFile(Annotations.Annotator.AnnotationFile file)
        {
            ClearAllElements();
            AddSessionLogData("Loading annotation file, please wait...");
            Tuple<bool, TimelineElementBuilder> res = LoadTimelineElements(file);
            bool aborted = false;
            string message = "Resource file was not found";
            string caption = "Resource file not found";
            while (!aborted && !res.Item1)
            {
                HashSet<string> matches = dataController.SearchForFile(Path.GetFileName(res.Item2.Filepath));
                if (matches.Count == 1)
                {
                    string match = matches.ToArray()[0];
                    res.Item2.ChangeFilepath(match);
                    AddSessionLogData(string.Format("Resource file {0} was not found at its specified path {1}, but it was automatically detected at location {2}. If this is incorrect, please remove this file.", Path.GetFileName(res.Item2.Filepath), res.Item2.Filepath, match));
                }
                else
                {
                    if (matches.Count > 0)
                    {
                        message = string.Format("{0} matches found, the file must be selected manually", matches.Count);
                        caption = "Multiple matches found.";
                    }
                    if (MessageBox.Show(string.Format(
                        "{2}: {0}{1}{1}To continue, click OK and select the appropriate file in its new location on the next screen. To abort, press Cancel.",
                        res.Item2.Filepath, Environment.NewLine, message),
                        caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.OK)
                    {
                        FileInfo f = new FileInfo(res.Item2.Filepath);
                        using (var fbd = new OpenFileDialog() { CheckFileExists = true, CheckPathExists = true, AutoUpgradeEnabled = true, Title = "Find file...", Filter = "All files |*" })
                        {
                            if (fbd.ShowDialog() == DialogResult.OK)
                            {
                                if (fbd.SafeFileName != Path.GetFileName(res.Item2.Filepath))
                                {
                                    if (MessageBox.Show(string.Format("The file you specified has a different name than the original missing file.{0}{0}Original file: {1}{0}Specified file: {2}{0}{0}Are you sure you want to proceed?",
                                        Environment.NewLine, Path.GetFileName(res.Item2.Filepath), fbd.SafeFileName), "Unexpected file name", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) != DialogResult.OK)
                                    {
                                        aborted = true;
                                    }
                                }
                                if (!aborted)
                                {
                                    res.Item2.ChangeFilepath(fbd.FileName);
                                    string path = Path.GetDirectoryName(Path.GetFullPath(fbd.FileName));
                                    if (path != null)
                                    {
                                        dataController.AddSearchPathFromUserInput(path);
                                    }
                                }
                            }
                            else
                            {
                                aborted = true;
                            }
                        }
                    }
                    else
                    {
                        aborted = true;
                    }
                }
                ClearAllElements(false);
                res = LoadTimelineElements(file);
            }
            if (aborted)
            {
                ClearAllElements(false);
                AddSessionLogData("The load operation was aborted.");
            }
            else
            {
                foreach (AnnotationKey key in file.AnnotationKeys.Values)
                {
                    AddAnnotationKeyToAnnotator(key.Key, key.Description);
                    AddAnnotationToMainForm(key.Key, key.Description);
                }
                foreach (Annotation ann in file.Annotations)
                {
                    if (!annotator.AnnotateKeyPress(ann.Key.Key, ann.Time))
                    {
                        throw new ArgumentException("Unregistered annotatation key or corrupted file.");
                    }
                    mainForm.AddAnnotation(annotator.LastAddedAnnotation);
                }
                foreach (var x in file.SessionKeyValues)
                {
                    annotator.SetSessionKeyValue(x.Key, x.Value);
                }
                AddSessionLogData("Annotation file loaded!");
            }
        }

        private Tuple<bool, TimelineElementBuilder> LoadTimelineElements(Annotations.Annotator.AnnotationFile file)
        {
            foreach (TimelineElementBase elem in file.TimelineElements)
            {
                TimelineElementBuilder builder = elem.CreateBuilder();
                if (!File.Exists(builder.Filepath))
                {
                    return new Tuple<bool, TimelineElementBuilder>(false, builder);
                }
                else
                {
                    Type builderType = builder.GetType();
                    if (builderType == typeof(VideoElement.VideoElementBuilder))
                    {
                        VideoElement.VideoElementBuilder video = builder as VideoElement.VideoElementBuilder;
                        video.BuildAndAdd(this);
                    }
                    else if (builderType == typeof(ChartElement.ChartElementBuilder))
                    {
                        ChartElement.ChartElementBuilder chart = builder as ChartElement.ChartElementBuilder;
                        chart.BuildAndAdd(this);
                    }
                    else
                    {
                        throw new ArgumentException("Unrecognized builder type: " + builderType.ToString());
                    }
                }
            }

            mainForm.SetTrackBarBoundValues(0, (int)Math.Max(0m,(timeline.EndPosition + 0.5m)));
            return new Tuple<bool, TimelineElementBuilder>(true, null);
        }

        public void LoadAnnotationFile(string p)
        {
            AddSessionLogData("Loading annotations, please wait...");
            try
            {
                Annotator.Annotations.Annotator.AnnotationFile f;
                try
                {
                    f = Annotations.Annotator.AnnotationFile.ReadFromFile(p);
                }
                catch (Exception ex)
                {
                    AddSessionLogData("Could not read annotation file " + p + ": " + ex.Message);
                    return;
                }
                LoadFromAnnotationFile(f);
            }
            catch (Exception ex)
            {
                AddSessionLogData("Could not apply annotation file " + p + ": " + ex.Message);
                ClearAllElements(false);
            }
        }

        public void RemoveAnnotationFromAnnotator(Annotation annotation)
        {
            annotator.RemoveAnnotation(annotation);
            AddSessionLogData("Annotation " + annotation.Key.Key.ToString() + " at time " + Utils.CreateSecondString(annotation.Time) + " was removed.");
        }

        public ICollection<string> DataColumnNames
        {
            get
            {
                return dataController.DataColumns;
            }
        }

        public void SkipToNextValueInRange(string dataColumn, decimal loThreshold, decimal hiThreshold)
        {
            try
            {
                decimal jumpTo = dataController.GetTimeWhenRangeReached(dataColumn, loThreshold, hiThreshold, timeline.CurrentPosition);
                if (jumpTo == -1m)
                {
                    AddSessionLogData("Range is never reached after current time.");
                }
                else
                {
                    Seek(jumpTo);
                }
            }
            catch (Exception e)
            {
                AddSessionLogData("Could not skip to next value: " + e.Message);
            }
        }

        public void SetSessionKeyValue(string key, string value)
        {
            SetSessionKeyValue(key, value, false);
        }

        public string GetSessionKeyValue(string key)
        {
            return annotator.GetSessionKeyValue(key);
        }

        public void SetSessionKeyValue(string key, string value, bool userInput)
        {
            try
            {
                if (userInput)
                {
                    string oldValue = annotator.GetSessionKeyValue(key);
                    if (oldValue != null)
                    {
                        DialogResult res = MessageBox.Show(string.Format("The key you are about to add is already associated with the value \"{0}\". If you continue with this operation, the value will be overwritten. Are you sure you want to proceed?", oldValue), "Are you sure?", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                        if (res != DialogResult.OK)
                        {
                            return;
                        }
                    }
                }
                annotator.SetSessionKeyValue(key, value);
            }
            catch (Exception e)
            {
                AddSessionLogData(string.Format("Could not set key-value \"{0}\"->\"{1}\": {2}", key, value, e.Message));
            }
        }

        public bool RemoveSessionKeyValue(string key)
        {
            try
            {
                return annotator.RemoveSessionKeyValue(key);
            }
            catch (Exception e)
            {
                AddSessionLogData(string.Format("Could not remove key-value for key \"{0}\": {2}", key, e.Message));
                return false;
            }
        }
    }
}
