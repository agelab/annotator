﻿using Annotator.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Annotator.Controllers
{
    public class PluginAdapter : IPluginHost
    {
        private readonly IPlugin pluginObject;
        private readonly IPluginData pluginData;
        private readonly string pluginDirectoryPath;
        private readonly ToolStripMenuItem basePluginMenuItem;
        private readonly ToolStripMenuItem enabledPluginMenuItem;
        private readonly MainController host;

        public PluginAdapter(MainController host, IPlugin pluginObject, IPluginData pluginData, string pluginDirectoryPath, ToolStripMenuItem basePluginMenuItem, ToolStripMenuItem enabledPluginMenuItem)
        {
            this.host = host;
            this.pluginObject = pluginObject;
            this.pluginData = pluginData;
            this.pluginDirectoryPath = pluginDirectoryPath;
            this.basePluginMenuItem = basePluginMenuItem;
            this.enabledPluginMenuItem = enabledPluginMenuItem;
            enabledPluginMenuItem.CheckedChanged += (x,y)=>SyncEnabledStateOnMenuItems();
        }

        public bool Enabled
        {
            get
            {
                return enabledPluginMenuItem.Checked;
            }
            set
            {
                enabledPluginMenuItem.Checked = value;
                SyncEnabledStateOnMenuItems();
            }
        }

        public IPlugin PluginObject
        {
            get
            {
                return pluginObject;
            }
        }

        public IPluginData PluginData
        {
            get
            {
                return pluginData;
            }
        }

        private void SyncEnabledStateOnMenuItems()
        {
            foreach (ToolStripMenuItem x in basePluginMenuItem.DropDownItems)
            {
                if (!Object.ReferenceEquals(x, enabledPluginMenuItem))
                {
                    x.Enabled = enabledPluginMenuItem.Checked;
                }
            }
        }

        #region IPluginHost Members

        public void AddSessionLogData(string data)
        {
            host.AddSessionLogData(data);
        }

        public string GetSessionKeyValue(string key)
        {
            return host.GetSessionKeyValue(key);
        }

        public void SetSessionKeyValue(string key, string value)
        {
            host.SetSessionKeyValue(key, value);
        }

        public bool RemoveSessionKeyValue(string key)
        {
            return host.RemoveSessionKeyValue(key);
        }

        public void AddPluginMenuItem(string text, Action callback)
        {
            ToolStripMenuItem item = new ToolStripMenuItem(text);
            item.Click += (x, y) =>
            {
                if (Enabled)
                {
                    callback();
                }
            };
            basePluginMenuItem.DropDownItems.Add(item);
        }

        public IEnumerable<string> GetSearchPaths()
        {
            return host.DataControllerInstance.SearchPaths;
        }

        public bool AddSearchPath(string path)
        {
            return host.DataControllerInstance.AddSearchPath(path);
        }

        public string GetPluginDirectoryPath()
        {
            return pluginDirectoryPath;
        }

        #endregion
    }
}
