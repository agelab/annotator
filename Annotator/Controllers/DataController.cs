﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using Annotator.Data;
using Annotator.Forms;
using Annotator.TimelineElements;
using OxyPlot.WindowsForms;

namespace Annotator.Controllers
{
    public class DataController
    {
        private readonly MainForm mainForm;
        private MainController mainController;

        private readonly Dictionary<string, Panel> videoPanels = new Dictionary<string,Panel>();
        private readonly Dictionary<string, VideoElement> videoElements = new Dictionary<string, VideoElement>();
        private readonly Dictionary<string, ChartElement> chartElements = new Dictionary<string, ChartElement>();
        private readonly Dictionary<string, AudioElement> audioElements = new Dictionary<string, AudioElement>();
        private readonly Dictionary<string, DataFile> dataFiles = new Dictionary<string, DataFile>();
        private readonly HashSet<string> searchPaths = new HashSet<string>();

        public DataController(MainForm mainForm, MainController mainController)
        {
            this.mainForm = mainForm;
            this.mainController = mainController;
        }

        public Dictionary<string, DataFile>.KeyCollection DataColumns
        {
            get
            {
                return dataFiles.Keys;
            }
        }

        public decimal GetTimeWhenRangeReached(string dataColumn, decimal loThreshold, decimal hiThreshold, decimal startTime = 0)
        {
            if (dataFiles.ContainsKey(dataColumn))
            {
                return dataFiles[dataColumn].GetTimeWhenRangeReached(dataColumn, loThreshold, hiThreshold, startTime);
            }
            else
            {
                throw new ArgumentException("Data column " + dataColumn + " does not exist.");
            }
        }

        public Plot AddChart(DataFile file, string dataColumnName, DragEventHandler enterDrag, DragEventHandler dragDrop, int height, int width)
        {
            dataFiles[dataColumnName] = file;
            ChartElement ce = new ChartElement(file.Filepath, file.TimingColumn, dataColumnName);
            ce.Chart.DragDrop += dragDrop;
            ce.Chart.DragEnter += enterDrag;
            ce.Chart.Height = height;
            ce.Chart.Width = width;
            ce.AddDataLine(dataColumnName, file.GetNormalizedTimeColumn(), file.GetColumn(dataColumnName));
            mainController.Timeline.AddElement(ce);
            mainForm.DataForm.AddChartSource(file.Filepath, dataColumnName, ce.EndPosition, 0);
            return ce.Chart;
        }

        public VideoElement AddVideo(string filepath, DragEventHandler enterDrag, DragEventHandler dragDrop, int minHeight, int minWidth, int maxHeight, int maxWidth)
        {
            if (videoElements.ContainsKey(filepath))
            {
                throw new ArgumentException(string.Format("File {0} has already been added.", filepath));
            }
            else
            {
                Panel p = new Panel();
                p.BackColor = Color.Black;
                p.BorderStyle = BorderStyle.FixedSingle;
                p.AllowDrop = true;
                p.DragEnter += enterDrag;
                p.DragDrop += dragDrop;
                VideoElement ve = new VideoElement(filepath, p);
                mainController.Timeline.AddElement(ve);
                ve.CurrentPosition = mainController.Timeline.CurrentPosition;
                //ResizePanel(p, ve.VideoHeight, ve.VideoWidth, minHeight, minWidth, maxHeight, maxWidth);
                ResizeControl(p, ve.VideoHeight, ve.VideoWidth, maxHeight);
                videoElements.Add(filepath, ve);
                videoPanels.Add(filepath, p);
                System.IO.FileInfo info = new System.IO.FileInfo(filepath);
                mainForm.DataForm.AddVideoSource(filepath, info.Name, ve.EndPosition, 0);
                return ve;
            }
        }

        public AudioElement AddAudio(string filepath)
        {
            if (audioElements.ContainsKey(filepath))
            {
                throw new ArgumentException(string.Format("File {0} has already been added.", filepath));
            }
            else
            {
                AudioElement ae = new AudioElement(filepath);
                mainController.Timeline.AddElement(ae);
                ae.CurrentPosition = mainController.Timeline.CurrentPosition;
                audioElements.Add(filepath, ae);
                System.IO.FileInfo info = new System.IO.FileInfo(filepath);
                mainForm.DataForm.AddAudioSource(filepath, info.Name, ae.EndPosition, 0);
                return ae;
            }
        }

        public void AddSearchPathFromUserInput(string path)
        {
            if(!AddSearchPath(path))
            {
                mainController.AddSessionLogData(string.Format("Cannot add search path, it does not point to an existing directory: {0}", path));
            }
        }

        public bool AddSearchPath(string path)
        {
            if (Directory.Exists(path))
            {
                searchPaths.Add(path);
                return true;
            }
            return false;
        }

        public IEnumerable<string> SearchPaths
        {
            get
            {
                return searchPaths;
            }
        }

        public void SetSearchPaths(string[] paths)
        {
            searchPaths.Clear();
            foreach(var s in paths)
            {
                AddSearchPathFromUserInput(s);
            }
        }

        public void ClearSearchPaths()
        {
            searchPaths.Clear();
        }

        /// <summary>
        /// Will search for the given filename in all search paths.
        /// </summary>
        /// <param name="filename">The name of the file to search for.</param>
        /// <returns>Returns a list of strings containing all matches.</returns>
        public HashSet<string> SearchForFile(string filename)
        {
            HashSet<string> matches = new HashSet<string>();
            foreach(var p in searchPaths)
            {
                foreach(var f in Directory.GetFiles(p, filename, SearchOption.AllDirectories))
                {
                    if (Path.GetFileName(f) == filename)
                    {
                        matches.Add(f);
                    }
                }
            }
            return matches;
        }

        public void ClearAllElements()
        {
            videoPanels.Clear();
            videoElements.Clear();
            chartElements.Clear();
            dataFiles.Clear();
        }

        public void RemoveVideo(string path)
        {
            if(videoPanels.ContainsKey(path))
            {
                mainController.RemoveVideo(videoPanels[path], path);
                videoPanels.Remove(path);
                videoElements.Remove(path);
            }
        }

        public void RemoveChart(string name)
        {
            if (chartElements.ContainsKey(name))
            {
                Plot chart = chartElements[name].Chart;
                if (chart.Model.Axes.Count == 1)
                {
                    mainController.RemoveChart(chart);
                }
                else
                {
                    throw new NotImplementedException();
                }
                string dataColumn = chartElements[name].DataColumnName;
                if (dataFiles.ContainsKey(dataColumn))
                {
                    dataFiles.Remove(dataColumn);
                }
                chartElements.Remove(name);
            }
        }

        public void RemoveAudio(string path)
        {
            if(audioElements.ContainsKey(path))
            {
                mainController.RemoveAudio(path);
                audioElements.Remove(path);
            }
        }

        public void ChangeAudioOffset(string path, decimal offset)
        {
            if(audioElements.ContainsKey(path))
            {
                audioElements[path].Offset = offset;
            }
        }

        public void ChangeVideoOffset(string path, decimal offset)
        {
            if (videoElements.ContainsKey(path))
            {
                videoElements[path].Offset = offset;
            }
        }

        public void ChangeChartOffset(string name, decimal offset)
        {
            if (chartElements.ContainsKey(name))
            {
                chartElements[name].Offset = offset;
            }
        }

        private static void ResizeControl(Control p, int videoHeight, int videoWidth, int height)
        {
            p.Height = height;
            p.Width = height * videoWidth / videoHeight;
        }

        private static void ResizeControl(Panel p, int videoHeight, int videoWidth, int minHeight, int minWidth, int maxHeight, int maxWidth)
        {
            if (videoHeight <= maxHeight && videoHeight >= minHeight)
            {
                p.Height = videoHeight;
            }
            else if (videoHeight > maxHeight)
            {
                p.Height = maxHeight;
            }
            else
            {
                //videoHeight < minHeight
                p.Height = minHeight;
            }

            if (videoWidth <= maxWidth && videoWidth >= minWidth)
            {
                p.Width = videoWidth;
            }
            else if (videoWidth > maxWidth)
            {
                p.Width = maxWidth;
            }
            else
            {
                //videoWidth < minWidth
                p.Width = minWidth;
            }
        }
    }
}
