﻿using Annotator.Plugins;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Annotator.Data;

namespace Annotator.Controllers
{
    public class PluginController
    {
        private const string PLUGIN_CATEGORY = "Plugins";
        private const string PLUGIN_ENABLED_PROPERTY = ".enabled";
        private const string EXCEPTION_TEXT = "Plugin \"{0}\" threw an exception: {1}";
        private readonly static string pluginsPath = Path.Combine(Directory.GetParent(typeof(Program).Assembly.Location).FullName, "plugins");

        private readonly MainController controller;
        private readonly Dictionary<string, PluginAdapter> plugins = new Dictionary<string, PluginAdapter>();

        private ToolStripMenuItem pluginBaseMenuItem;

        public PluginController(MainController controller)
        {
            this.controller = controller;
        }

        public void Initialize(ToolStripMenuItem pluginBaseMenuItem)
        {
            this.pluginBaseMenuItem = pluginBaseMenuItem;
            var importPluginMenuItem = new ToolStripMenuItem("Import plugin from file...");
            importPluginMenuItem.Click += importPluginMenuItem_Click;
            pluginBaseMenuItem.DropDownItems.Add(importPluginMenuItem);
            InitializePluginList();
        }

        private void importPluginMenuItem_Click(object sender, EventArgs e)
        {
            using(OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.AddExtension = true;
                ofd.Filter = "Executable files (*.dll, *.exe)|*.dll;*.exe|All files|*.*";
                ofd.CheckFileExists = true;
                ofd.Multiselect = true;
                ofd.Title = "Select file where plugins are located";
                if(ofd.ShowDialog() == DialogResult.OK)
                {
                    int complete = 0;
                    controller.AddSessionLogData(string.Format("Adding {0} files to the plugins directory. If they contain valid plugins, the plugins will appear in the Plugins menu as soon as the operation is complete.", ofd.FileNames.Length));
                    foreach(var file in ofd.FileNames)
                    {
                        try
                        {
                            ImportPluginFromFile(file);
                            complete++;
                        }
                        catch (Exception ex)
                        {
                            controller.AddSessionLogData(string.Format("File {0} could not be copied to the plugins directory: {1}", file, ex.Message));
                        }
                    }
                    controller.AddSessionLogData(string.Format("Plugin operation complete: {0} files were copied to the plugins directory.", complete));
                }
            }
        }

        private class PluginList
        {
            [ImportMany]
            public IEnumerable<Lazy<IPlugin, IPluginData>> plugins;
        }
        
        private void InitializePluginList()
        {
            using (var catalog = new AggregateCatalog())
            {
                //Adds all the parts found in the same assembly as the Program class
                catalog.Catalogs.Add(new AssemblyCatalog(typeof(Program).Assembly));
                if (Directory.Exists(pluginsPath))
                {
                    catalog.Catalogs.Add(new DirectoryCatalog(pluginsPath));
                }
                using(var container = new CompositionContainer(catalog))
                {
                    PluginList list = new PluginList();
                    container.ComposeParts(list);

                    foreach (var x in list.plugins)
                    {
                        var host = CreatePluginObject(x);
                        controller.AddSessionLogData(string.Format("Initializing plugin \"{0}\"", x.Metadata.Name));
                        try
                        {
                            x.Value.Initialize(host);
                            plugins.Add(x.Metadata.Name, host);
                        }
                        catch(Exception ex)
                        {
                            controller.AddSessionLogData(string.Format("Plugin \"{0}\" threw an exception: {1}", x.Metadata.Name, ex.ToString()));
                        }
                    }
                }
            }
        }

        private void RefreshPluginList()
        {
            using (var catalog = new AggregateCatalog())
            {
                //Adds all the parts found in the same assembly as the Program class
                catalog.Catalogs.Add(new AssemblyCatalog(typeof(Program).Assembly));
                if (Directory.Exists(pluginsPath))
                {
                    catalog.Catalogs.Add(new DirectoryCatalog(pluginsPath));
                }
                using (var container = new CompositionContainer(catalog))
                {
                    PluginList list = new PluginList();
                    container.ComposeParts(list);

                    foreach (var x in list.plugins)
                    {
                        if (!plugins.ContainsKey(x.Metadata.Name))
                        {
                            var host = CreatePluginObject(x);
                            controller.AddSessionLogData(string.Format("Initializing new plugin \"{0}\"", x.Metadata.Name));
                            try
                            {
                                x.Value.Initialize(host);
                                plugins.Add(x.Metadata.Name, host);
                            }
                            catch (Exception ex)
                            {
                                controller.AddSessionLogData(string.Format(EXCEPTION_TEXT, x.Metadata.Name, ex.ToString()));
                            }
                        }
                    }
                }
            }
        }

        public void ImportPluginFromFile(string filepath)
        {
            if(!Directory.Exists(pluginsPath))
            {
                Directory.CreateDirectory(pluginsPath);
            }
            FileInfo info = new FileInfo(filepath);
            File.Copy(filepath, Path.Combine(pluginsPath, info.Name));
            RefreshPluginList();
        }

        private PluginAdapter CreatePluginObject(Lazy<IPlugin, IPluginData> x)
        {
            var basePluginMenuItem = new ToolStripMenuItem(x.Metadata.Name);
            pluginBaseMenuItem.DropDownItems.Add(basePluginMenuItem);
            var enabledPluginMenuItem = new ToolStripMenuItem("Enabled") { CheckOnClick = true, Checked = true, CheckState = CheckState.Checked  };
            basePluginMenuItem.DropDownItems.Add(enabledPluginMenuItem);
            string path = Path.Combine(pluginsPath, x.Metadata.Name);
            if(!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            PluginAdapter p = new PluginAdapter(controller, x.Value, x.Metadata, path, basePluginMenuItem, enabledPluginMenuItem);
            return p;
        }

        internal void OnSavingAnnotations(Annotations.Annotator.AnnotationFile data)
        {
            foreach(var x in plugins)
            {
                if(x.Value.Enabled)
                {
                    try
                    {
                        x.Value.PluginObject.OnSavingAnnotations(data);
                    }
                    catch(Exception ex)
                    {
                        controller.AddSessionLogData(string.Format(EXCEPTION_TEXT, x.Key, ex.ToString()));
                    }
                }
            }
        }

        internal void OnSavedAnnotations(Annotations.Annotator.AnnotationFile data, string filepath)
        {
            foreach (var x in plugins)
            {
                if (x.Value.Enabled)
                {
                    try
                    {
                        x.Value.PluginObject.OnSavedAnnotations(data, filepath);
                    }
                    catch (Exception ex)
                    {
                        controller.AddSessionLogData(string.Format(EXCEPTION_TEXT, x.Key, ex.ToString()));
                    }
                }
            }
        }

        internal void OnApplicationClosed()
        {
            foreach (var x in plugins)
            {
                if (x.Value.Enabled)
                {
                    try
                    {
                        x.Value.PluginObject.OnApplicationClosed();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(string.Format(EXCEPTION_TEXT, x.Key, ex.ToString()), "Plugin exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        internal void SaveProperties(IniFile file)
        {
            foreach (var x in plugins)
            {
                file.WriteValue(PLUGIN_CATEGORY, x.Key + PLUGIN_ENABLED_PROPERTY, x.Value.Enabled.ToString());
            }
        }

        internal bool ApplyProperties(IniFile file, List<string> errorMessages)
        {
            bool value;
            foreach(var x in plugins)
            {
                if(bool.TryParse(file.ReadValue(PLUGIN_CATEGORY, x.Key + PLUGIN_ENABLED_PROPERTY),out value))
                {
                    x.Value.Enabled = value;
                }
            }
            return true;
        }
    }
}
