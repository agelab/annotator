﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Annotator.Data;

namespace Annotator.Controllers
{
    public class ShortcutController
    {

        private const String SHORTCUT_CATEGORY = "Shortcuts";
        private const String PLAY_SHORTCUT_PROPERTY = "toggle.play";
        private const String PUSH_TO_PLAY_SHORTCUT_PROPERTY = "pushto.play";
        private const String STEPFORWARD_SHORTCUT_PROPERTY = "step.forward";
        private const String STEPBACK_SHORTCUT_PROPERTY = "step.backward";
        private const String JUMPFORWARD_SHORTCUT_PROPERTY = "jump.forward";
        private const String JUMPBACK_SHORTCUT_PROPERTY = "jump.backward";
        private const String RATE_INCREASE_SHORTCUT_PROPERTY = "rate.increase";
        private const String RATE_DECREASE_SHORTCUT_PROPERTY = "rate.decrease";
        private const String SAVE_ANNOTATIONS_SHORTCUT_PROPERTY = "annotations.save";

        private const int SHORTCUT_COUNT = 9;

        private static readonly Dictionary<String, ShortcutKeyType> properties = new Dictionary<string, ShortcutKeyType>()
        {
            {PLAY_SHORTCUT_PROPERTY,            ShortcutKeyType.PlayPause},
            {STEPFORWARD_SHORTCUT_PROPERTY,     ShortcutKeyType.StepForward},
            {STEPBACK_SHORTCUT_PROPERTY,        ShortcutKeyType.StepBackward},
            {JUMPFORWARD_SHORTCUT_PROPERTY,     ShortcutKeyType.JumpForward},
            {JUMPBACK_SHORTCUT_PROPERTY,        ShortcutKeyType.JumpBackward},
            {RATE_INCREASE_SHORTCUT_PROPERTY,   ShortcutKeyType.IncreaseRate},
            {RATE_DECREASE_SHORTCUT_PROPERTY,   ShortcutKeyType.DecreaseRate},
            {SAVE_ANNOTATIONS_SHORTCUT_PROPERTY,ShortcutKeyType.SaveAnnotations},
            {PUSH_TO_PLAY_SHORTCUT_PROPERTY,    ShortcutKeyType.PushToPlay}
        };

        private readonly Dictionary<Keys, ShortcutKeyType> shortcutAction = new Dictionary<Keys, ShortcutKeyType>();
        private readonly Action<object,EventArgs>[] action;
        private readonly Dictionary<Keys, Action> fixedShortcuts = new Dictionary<Keys,Action>();
        private readonly Keys[] shortcutKey;
        private static readonly Keys[] defaultShortcuts = new Keys[SHORTCUT_COUNT] { Keys.Space,
                                                                                     Keys.Right,
                                                                                     Keys.Left,
                                                                                     Keys.Up,
                                                                                     Keys.Down,
                                                                                     Keys.OemCloseBrackets,
                                                                                     Keys.OemOpenBrackets,
                                                                                     Keys.S,
                                                                                     Keys.P
                                                                    };

        private ShortcutController(MainController ctrl, Keys[] shortcuts)  //shortcuts must have 8 elements
        {
            shortcutKey = new Keys[SHORTCUT_COUNT];
            Array.Copy(shortcuts, shortcutKey, SHORTCUT_COUNT);
            for (int i = 0; i < SHORTCUT_COUNT; i++)
            {
                shortcutAction.Add(shortcutKey[i], (ShortcutKeyType)i);
            }

            action = new Action<object, EventArgs>[SHORTCUT_COUNT] {
                ctrl.PlayPause, 
                ctrl.StepForward, 
                ctrl.StepBackward, 
                ctrl.JumpForward, 
                ctrl.JumpBackward, 
                ctrl.IncreaseRate, 
                ctrl.DecreaseRate, 
                ctrl.SaveAnnotations,
                ctrl.PushToPlay
            };

        }

        public bool ApplyProperties(IniFile file, MainController ctrl, List<String> errorMessages)
        {
            bool applied = true;
            int value;
            foreach(var x in properties)
            {
                if(int.TryParse(file.ReadValue(SHORTCUT_CATEGORY,x.Key),out value))
                {
                    applied &= ChangeShortcut(x.Value, (Keys)value, ctrl);
                }
            }
            return applied;
        }

        public void SaveProperties(IniFile file)
        {
            foreach (var x in properties)
            {
                file.WriteValue(SHORTCUT_CATEGORY, x.Key, ((int)shortcutKey[(int)x.Value]).ToString());
            }
        }

        public static ShortcutController CreateDefaultInstance(MainController ctrl)
        {
            return new ShortcutController(ctrl,defaultShortcuts);
        }

        public static ShortcutController CreateCustomInstance(MainController ctrl, Dictionary<ShortcutKeyType,Keys> shortcuts)
        {
            Keys[] k = new Keys[SHORTCUT_COUNT];
            for(int i=0;i<SHORTCUT_COUNT;i++)
            {
                if(shortcuts.ContainsKey((ShortcutKeyType)i))
                {
                    k[i] = shortcuts[(ShortcutKeyType)i];
                } else {
                    k[i] = defaultShortcuts[i];
                }
            }
            return new ShortcutController(ctrl, k);
        }

        public bool ProcessKeyPress(object sender, EventArgs e, Keys key)
        {
            bool res;
            if(res = fixedShortcuts.ContainsKey(key))
            {
                fixedShortcuts[key].Invoke();
            }
            else if (res = shortcutAction.ContainsKey(key))
            {
                action[(int)shortcutAction[key]].Invoke(sender,e);
            }
            return res;
        }

        public void RegisterFixedShortcut(Keys key, Action action)
        {
            fixedShortcuts[key] = action;
        }

        public void UnregisterFixedShortcut(Keys key)
        {
            fixedShortcuts.Remove(key);
        }

        public void ClearFixedShortcuts()
        {
            fixedShortcuts.Clear();
        }

        public bool ChangeShortcut(ShortcutKeyType type, Keys newKey, MainController ctrl)
        {
            int id = (int)type;
            bool res;

            if (newKey == shortcutKey[(int)type])
            {
                //no reassignment necessary
                return true;
            }

            if (!(res = shortcutAction.ContainsKey(newKey)))
            {
                Keys oldKey = shortcutKey[id];
                shortcutAction.Remove(oldKey);
                shortcutAction.Add(newKey, type);
                shortcutKey[id] = newKey;
            }
            return !res;
        }

        public Dictionary<Keys, ShortcutKeyType>.KeyCollection ReservedKeys
        {
            get
            {
                return shortcutAction.Keys;
            }
        }

        public Keys GetKeyForAction(ShortcutKeyType type)
        {
            return shortcutKey[(int)type];
        }

    }

    public enum ShortcutKeyType
    {
        PlayPause = 0,
        StepForward = 1,
        StepBackward = 2,
        JumpForward = 3,
        JumpBackward = 4,
        IncreaseRate = 5,
        DecreaseRate = 6,
        SaveAnnotations = 7,
        PushToPlay = 8
    }
}
