﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Annotator.Controllers;
using Annotator.Utility;
using OxyPlot.WindowsForms;

namespace Annotator.Forms
{
    public partial class ChartForm : Form
    {
        
        private readonly MainController controller;

        public ChartForm(MainController controller)
        {
            this.controller = controller;
            InitializeComponent();
            Utils.ApplyToControlAndChildren(this, (x) =>
            {
                x.AllowDrop = true;
                x.DragEnter += controller.DragEnterEventHandler;
                x.DragDrop += controller.DragDropEventHandler;
            });
        }

        public void AddChart(Plot chart)
        {
            this.Visible = true;
            flowLayoutPanel1.Controls.Add(chart);
        }

        public void RemoveAllCharts()
        {
            foreach (Control c in flowLayoutPanel1.Controls)
            {
                c.Dispose();
            }
            flowLayoutPanel1.Controls.Clear();
        }

        public void RemoveChart(Plot chart)
        {
            flowLayoutPanel1.Controls.Remove(chart);
            chart.Dispose();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visible = false;
 	        base.OnClosing(e);
        }

    }
}
