﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Annotator.Controllers;
using Annotator.Controls;
using Annotator.Utility;

namespace Annotator.Forms
{
    public partial class DataForm : Form
    {

        private readonly MainController controller;

        public DataForm(MainController controller)
        {
            this.controller = controller;
            InitializeComponent();
            Utils.ApplyToControlAndChildren(this, (x) =>
            {
                x.AllowDrop = true;
                x.DragEnter += controller.DragEnterEventHandler;
                x.DragDrop += controller.DragDropEventHandler;
            });
        }

        public void InitializeDataBindings(BindingSource keyValueBindingSource, Func<string, bool> validityCondition)
        {
            this.keyValueDataGridView.DataSource = keyValueBindingSource;
            this.keyTextBox.ValidityCondition = validityCondition;
        }

        public void AddVideoSource(string path, string name, decimal length, decimal offset)
        {
            VideoInfoPanel videoInfo = new VideoInfoPanel();
            videoInfo.SetNewVideoName(path, name);
            videoInfo.VideoLength = Utils.CreateTimeString(length).ToString();
            videoInfo.Offset = offset;
            videoInfo.Removed += new EventHandler(controller.VideoRemoved);
            videoInfo.OffsetChanged += new VideoInfoPanel.OffsetChangedEventHandler<decimal>(controller.VideoOffsetChanged);
            videoInfo.AddKeyDownHandler(controller.KeyDownEventHandler);
            flowLayoutPanel1.Controls.Add(videoInfo);
        }

        public void AddAudioSource(string path, string name, decimal length, decimal offset)
        {
            VideoInfoPanel audioInfo = new VideoInfoPanel();
            audioInfo.SetNewVideoName(path, name);
            audioInfo.VideoLength = Utils.CreateTimeString(length).ToString();
            audioInfo.Offset = offset;
            audioInfo.Removed += new EventHandler(controller.AudioRemoved);
            audioInfo.OffsetChanged += new VideoInfoPanel.OffsetChangedEventHandler<decimal>(controller.AudioOffsetChanged);
            audioInfo.AddKeyDownHandler(controller.KeyDownEventHandler);
            flowLayoutPanel1.Controls.Add(audioInfo);
        }

        public void RemoveMediaInfoPanel(MediaInfoPanel p)
        {
            p.RemoveKeyDownHandler(controller.KeyDownEventHandler);
            flowLayoutPanel1.Controls.Remove(p);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            controller.FocusMainForm();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visible = false;
 	        base.OnClosing(e);
        }

        public void AddChartSource(string path, string dataColumnName, decimal length, decimal offset)
        {
            ChartInfoPanel chartInfo = new ChartInfoPanel();
            chartInfo.SetNewChartName(path, dataColumnName);
            chartInfo.Offset = offset;
            chartInfo.Removed += new EventHandler(controller.ChartRemoved);
            chartInfo.OffsetChanged += new ChartInfoPanel.OffsetChangedEventHandler<decimal>(controller.ChartOffsetChanged);
            chartInfo.AddKeyDownHandler(controller.KeyDownEventHandler);
            flowLayoutPanel1.Controls.Add(chartInfo);
        }

        public void ClearAllElements()
        {
            flowLayoutPanel1.Controls.Clear();
        }

        private void addKeyValueButton_Click(object sender, EventArgs e)
        {
            if (keyTextBox.TextLength > 0)
            {
                controller.SetSessionKeyValue(keyTextBox.Text, valueTextBox.Text, true);
                keyTextBox.RefreshValidity();
            }
        }

        private void keyValueDataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if(keyValueDataGridView.SelectedRows.Count > 0 && (e.KeyData == Keys.Delete || e.KeyData == Keys.Back))
            {
                removeSelectedToolStripMenuItem_Click(sender, e);
            }
        }

        private void removeSelectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var res = MessageBox.Show("Are you sure you want to remove the selected key-value pairs?", "Are you sure?", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if(res != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }
            foreach (DataGridViewRow row in keyValueDataGridView.SelectedRows)
            {
                controller.RemoveSessionKeyValue(row.Cells[0].Value as string);
            }
            keyTextBox.RefreshValidity();
        }

        private void keyValueContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            e.Cancel = keyValueDataGridView.SelectedRows.Count == 0;
            removeSelectedToolStripMenuItem.Enabled = keyValueDataGridView.SelectedRows.Count > 0;
        }

    }
}
