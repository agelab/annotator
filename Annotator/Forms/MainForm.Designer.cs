﻿namespace Annotator.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.positionTrackBar = new System.Windows.Forms.TrackBar();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.playStateLabel = new System.Windows.Forms.Label();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.sessionLogTextBox = new System.Windows.Forms.TextBox();
            this.annotationsListView = new ListViewEmbeddedControls.ListViewEx();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.annotationKeysLabel = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataSourcesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAnnotationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadAnnotationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearAnnotationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importPreferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportPreferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeLastAnnotationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearWorkspaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playbackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.skipToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.dataColumnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lowerThresholdValueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.upperThresholdValueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playPauseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stepForwardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stepBackwardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jumpForwardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jumpBackwardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.increaseRateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.decreaseRateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pluginsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pushToPlayShortcutTextBox = new Annotator.Controls.ShortcutTextBox();
            this.decrRateShortcutTextBox = new Annotator.Controls.ShortcutTextBox();
            this.stepBackShortcutTextBox = new Annotator.Controls.ShortcutTextBox();
            this.jumpFwShortcutTextBox = new Annotator.Controls.ShortcutTextBox();
            this.incrRateShortcutTextBox = new Annotator.Controls.ShortcutTextBox();
            this.jumpBackShortcutTextBox = new Annotator.Controls.ShortcutTextBox();
            this.stepFwShortcutTextBox = new Annotator.Controls.ShortcutTextBox();
            this.playShortcutTextBox = new Annotator.Controls.ShortcutTextBox();
            this.saveShortcutTextBox = new Annotator.Controls.ShortcutTextBox();
            this.addAnnotationButton = new Annotator.Controls.KeyInputButton();
            this.dataColumnComboBox = new Annotator.Controls.ToolStripUpdateableComboBox();
            this.lowerThresholdTextBox = new Annotator.Controls.ToolStripNumericTextBox();
            this.upperThresholdTextBox = new Annotator.Controls.ToolStripNumericTextBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.positionTrackBar)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.positionTrackBar, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(760, 898);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(754, 808);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // positionTrackBar
            // 
            this.positionTrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.positionTrackBar.Location = new System.Drawing.Point(3, 814);
            this.positionTrackBar.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.positionTrackBar.Name = "positionTrackBar";
            this.positionTrackBar.Size = new System.Drawing.Size(754, 45);
            this.positionTrackBar.TabIndex = 0;
            this.positionTrackBar.TickFrequency = 0;
            this.positionTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel2, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 859);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(760, 39);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.Controls.Add(this.button1);
            this.flowLayoutPanel2.Controls.Add(this.button2);
            this.flowLayoutPanel2.Controls.Add(this.button3);
            this.flowLayoutPanel2.Controls.Add(this.button4);
            this.flowLayoutPanel2.Controls.Add(this.button5);
            this.flowLayoutPanel2.Controls.Add(this.button6);
            this.flowLayoutPanel2.Controls.Add(this.button7);
            this.flowLayoutPanel2.Controls.Add(this.playStateLabel);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(167, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(426, 33);
            this.flowLayoutPanel2.TabIndex = 1;
            this.flowLayoutPanel2.WrapContents = false;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.button1.Location = new System.Drawing.Point(3, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(32, 30);
            this.button1.TabIndex = 0;
            this.button1.Text = "<<";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.button2.Location = new System.Drawing.Point(41, 0);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(32, 30);
            this.button2.TabIndex = 1;
            this.button2.Text = "|<";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.button3.Location = new System.Drawing.Point(79, 0);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(32, 30);
            this.button3.TabIndex = 2;
            this.button3.Text = "<";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.button4.Location = new System.Drawing.Point(117, 0);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(32, 30);
            this.button4.TabIndex = 3;
            this.button4.Text = "|>";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.button5.Location = new System.Drawing.Point(155, 0);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(32, 30);
            this.button5.TabIndex = 4;
            this.button5.Text = ">";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.button6.Location = new System.Drawing.Point(193, 0);
            this.button6.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(32, 30);
            this.button6.TabIndex = 6;
            this.button6.Text = ">|";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.button7.Location = new System.Drawing.Point(231, 0);
            this.button7.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(32, 30);
            this.button7.TabIndex = 7;
            this.button7.Text = ">>";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // playStateLabel
            // 
            this.playStateLabel.AutoSize = true;
            this.playStateLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.playStateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.playStateLabel.Location = new System.Drawing.Point(269, 7);
            this.playStateLabel.Margin = new System.Windows.Forms.Padding(3, 7, 3, 7);
            this.playStateLabel.Name = "playStateLabel";
            this.playStateLabel.Size = new System.Drawing.Size(154, 19);
            this.playStateLabel.TabIndex = 5;
            this.playStateLabel.Text = "00:00.00 / 0.00 @ 1.0x";
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 24);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(1115, 902);
            this.splitContainer2.SplitterDistance = 764;
            this.splitContainer2.TabIndex = 1;
            // 
            // splitContainer3
            // 
            this.splitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.splitContainer4);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.tableLayoutPanel3);
            this.splitContainer3.Size = new System.Drawing.Size(347, 902);
            this.splitContainer3.SplitterDistance = 697;
            this.splitContainer3.TabIndex = 0;
            this.splitContainer3.TabStop = false;
            // 
            // splitContainer4
            // 
            this.splitContainer4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.tableLayoutPanel4);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.annotationsListView);
            this.splitContainer4.Size = new System.Drawing.Size(347, 697);
            this.splitContainer4.SplitterDistance = 262;
            this.splitContainer4.TabIndex = 0;
            this.splitContainer4.TabStop = false;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.sessionLogTextBox, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(343, 258);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(337, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Session log:";
            // 
            // sessionLogTextBox
            // 
            this.sessionLogTextBox.BackColor = System.Drawing.Color.White;
            this.sessionLogTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.sessionLogTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sessionLogTextBox.Location = new System.Drawing.Point(0, 23);
            this.sessionLogTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.sessionLogTextBox.Multiline = true;
            this.sessionLogTextBox.Name = "sessionLogTextBox";
            this.sessionLogTextBox.ReadOnly = true;
            this.sessionLogTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sessionLogTextBox.Size = new System.Drawing.Size(343, 235);
            this.sessionLogTextBox.TabIndex = 6;
            this.sessionLogTextBox.TabStop = false;
            // 
            // annotationsListView
            // 
            this.annotationsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.annotationsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.annotationsListView.Location = new System.Drawing.Point(0, 0);
            this.annotationsListView.MultiSelect = false;
            this.annotationsListView.Name = "annotationsListView";
            this.annotationsListView.Size = new System.Drawing.Size(343, 427);
            this.annotationsListView.TabIndex = 0;
            this.annotationsListView.UseCompatibleStateImageBehavior = false;
            this.annotationsListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Annotation Key";
            this.columnHeader1.Width = 104;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Time (mm:ss.cc)";
            this.columnHeader2.Width = 88;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Time (s)";
            this.columnHeader3.Width = 86;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Delete";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AutoScroll = true;
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.pushToPlayShortcutTextBox, 2, 5);
            this.tableLayoutPanel3.Controls.Add(this.label12, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.decrRateShortcutTextBox, 4, 4);
            this.tableLayoutPanel3.Controls.Add(this.stepBackShortcutTextBox, 4, 2);
            this.tableLayoutPanel3.Controls.Add(this.jumpFwShortcutTextBox, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.incrRateShortcutTextBox, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.jumpBackShortcutTextBox, 4, 3);
            this.tableLayoutPanel3.Controls.Add(this.stepFwShortcutTextBox, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.playShortcutTextBox, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label5, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label6, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.label7, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label8, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.label9, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.label10, 3, 4);
            this.tableLayoutPanel3.Controls.Add(this.saveShortcutTextBox, 4, 1);
            this.tableLayoutPanel3.Controls.Add(this.annotationKeysLabel, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.addAnnotationButton, 3, 6);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 7;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(343, 197);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.label12, 2);
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label12.Location = new System.Drawing.Point(4, 129);
            this.label12.Margin = new System.Windows.Forms.Padding(4);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 17);
            this.label12.TabIndex = 19;
            this.label12.Text = "Push to play:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.label2, 5);
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label2.Location = new System.Drawing.Point(3, 3);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 3, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(255, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Control Shortcuts (click box to change):";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.label3, 2);
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label3.Location = new System.Drawing.Point(4, 29);
            this.label3.Margin = new System.Windows.Forms.Padding(4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Play/Pause:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label5.Location = new System.Drawing.Point(179, 29);
            this.label5.Margin = new System.Windows.Forms.Padding(4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 17);
            this.label5.TabIndex = 3;
            this.label5.Text = "Save data...:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.label4, 2);
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label4.Location = new System.Drawing.Point(4, 54);
            this.label4.Margin = new System.Windows.Forms.Padding(4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "Step forward:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label6.Location = new System.Drawing.Point(179, 54);
            this.label6.Margin = new System.Windows.Forms.Padding(4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 17);
            this.label6.TabIndex = 4;
            this.label6.Text = "Step back:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.label7, 2);
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label7.Location = new System.Drawing.Point(4, 79);
            this.label7.Margin = new System.Windows.Forms.Padding(4);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 17);
            this.label7.TabIndex = 5;
            this.label7.Text = "Jump forward:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label8.Location = new System.Drawing.Point(179, 79);
            this.label8.Margin = new System.Windows.Forms.Padding(4);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 17);
            this.label8.TabIndex = 6;
            this.label8.Text = "Jump back:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.label9, 2);
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label9.Location = new System.Drawing.Point(4, 104);
            this.label9.Margin = new System.Windows.Forms.Padding(4);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 17);
            this.label9.TabIndex = 7;
            this.label9.Text = "Increase rate:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label10.Location = new System.Drawing.Point(179, 104);
            this.label10.Margin = new System.Windows.Forms.Padding(4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(102, 17);
            this.label10.TabIndex = 8;
            this.label10.Text = "Decrease rate:";
            // 
            // annotationKeysLabel
            // 
            this.annotationKeysLabel.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.annotationKeysLabel, 2);
            this.annotationKeysLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.annotationKeysLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.annotationKeysLabel.Location = new System.Drawing.Point(3, 162);
            this.annotationKeysLabel.Margin = new System.Windows.Forms.Padding(3, 12, 3, 5);
            this.annotationKeysLabel.Name = "annotationKeysLabel";
            this.annotationKeysLabel.Size = new System.Drawing.Size(111, 30);
            this.annotationKeysLabel.TabIndex = 17;
            this.annotationKeysLabel.Text = "Annotation Keys";
            // 
            // timer1
            // 
            this.timer1.Interval = 30;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.playbackToolStripMenuItem,
            this.pluginsToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1115, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataSourcesToolStripMenuItem,
            this.saveAnnotationsToolStripMenuItem,
            this.loadAnnotationsToolStripMenuItem,
            this.clearAnnotationsToolStripMenuItem,
            this.importPreferencesToolStripMenuItem,
            this.exportPreferencesToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // dataSourcesToolStripMenuItem
            // 
            this.dataSourcesToolStripMenuItem.Name = "dataSourcesToolStripMenuItem";
            this.dataSourcesToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.dataSourcesToolStripMenuItem.Text = "Data sources...";
            this.dataSourcesToolStripMenuItem.Click += new System.EventHandler(this.dataSourcesToolStripMenuItem_Click);
            // 
            // saveAnnotationsToolStripMenuItem
            // 
            this.saveAnnotationsToolStripMenuItem.Name = "saveAnnotationsToolStripMenuItem";
            this.saveAnnotationsToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.saveAnnotationsToolStripMenuItem.Text = "Save annotations...";
            // 
            // loadAnnotationsToolStripMenuItem
            // 
            this.loadAnnotationsToolStripMenuItem.Name = "loadAnnotationsToolStripMenuItem";
            this.loadAnnotationsToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.loadAnnotationsToolStripMenuItem.Text = "Load annotations...";
            this.loadAnnotationsToolStripMenuItem.Click += new System.EventHandler(this.loadAnnotationsToolStripMenuItem_Click);
            // 
            // clearAnnotationsToolStripMenuItem
            // 
            this.clearAnnotationsToolStripMenuItem.Name = "clearAnnotationsToolStripMenuItem";
            this.clearAnnotationsToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.clearAnnotationsToolStripMenuItem.Text = "Clear annotations";
            this.clearAnnotationsToolStripMenuItem.Click += new System.EventHandler(this.clearAnnotationsToolStripMenuItem_Click);
            // 
            // importPreferencesToolStripMenuItem
            // 
            this.importPreferencesToolStripMenuItem.Name = "importPreferencesToolStripMenuItem";
            this.importPreferencesToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.importPreferencesToolStripMenuItem.Text = "Import preferences...";
            this.importPreferencesToolStripMenuItem.Click += new System.EventHandler(this.importPreferencesToolStripMenuItem_Click);
            // 
            // exportPreferencesToolStripMenuItem
            // 
            this.exportPreferencesToolStripMenuItem.Name = "exportPreferencesToolStripMenuItem";
            this.exportPreferencesToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.exportPreferencesToolStripMenuItem.Text = "Export preferences...";
            this.exportPreferencesToolStripMenuItem.Click += new System.EventHandler(this.exportPreferencesToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeLastAnnotationToolStripMenuItem,
            this.clearWorkspaceToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // removeLastAnnotationToolStripMenuItem
            // 
            this.removeLastAnnotationToolStripMenuItem.Name = "removeLastAnnotationToolStripMenuItem";
            this.removeLastAnnotationToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Z";
            this.removeLastAnnotationToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.removeLastAnnotationToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.removeLastAnnotationToolStripMenuItem.Text = "Remove last annotation";
            this.removeLastAnnotationToolStripMenuItem.MouseDown += new System.Windows.Forms.MouseEventHandler(this.removeLastAnnotationToolStripMenuItem_MouseDown);
            // 
            // clearWorkspaceToolStripMenuItem
            // 
            this.clearWorkspaceToolStripMenuItem.Name = "clearWorkspaceToolStripMenuItem";
            this.clearWorkspaceToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.clearWorkspaceToolStripMenuItem.Text = "Clear workspace";
            this.clearWorkspaceToolStripMenuItem.Click += new System.EventHandler(this.clearWorkspaceToolStripMenuItem_Click);
            // 
            // playbackToolStripMenuItem
            // 
            this.playbackToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.skipToToolStripMenuItem,
            this.playPauseToolStripMenuItem,
            this.stepForwardToolStripMenuItem,
            this.stepBackwardToolStripMenuItem,
            this.jumpForwardToolStripMenuItem,
            this.jumpBackwardToolStripMenuItem,
            this.increaseRateToolStripMenuItem,
            this.decreaseRateToolStripMenuItem});
            this.playbackToolStripMenuItem.Name = "playbackToolStripMenuItem";
            this.playbackToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.playbackToolStripMenuItem.Text = "Playback";
            // 
            // skipToToolStripMenuItem
            // 
            this.skipToToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripSeparator1,
            this.dataColumnToolStripMenuItem,
            this.lowerThresholdValueToolStripMenuItem,
            this.upperThresholdValueToolStripMenuItem});
            this.skipToToolStripMenuItem.Name = "skipToToolStripMenuItem";
            this.skipToToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.skipToToolStripMenuItem.Text = "Skip to...";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.toolStripMenuItem1.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItem1.Text = "Next value in range";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(215, 6);
            // 
            // dataColumnToolStripMenuItem
            // 
            this.dataColumnToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataColumnComboBox});
            this.dataColumnToolStripMenuItem.Name = "dataColumnToolStripMenuItem";
            this.dataColumnToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.dataColumnToolStripMenuItem.Text = "Data column...";
            this.dataColumnToolStripMenuItem.DropDownOpening += new System.EventHandler(this.dataColumnToolStripMenuItem_DropDownOpening);
            // 
            // lowerThresholdValueToolStripMenuItem
            // 
            this.lowerThresholdValueToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lowerThresholdTextBox});
            this.lowerThresholdValueToolStripMenuItem.Name = "lowerThresholdValueToolStripMenuItem";
            this.lowerThresholdValueToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.lowerThresholdValueToolStripMenuItem.Text = "Lower threshold value...";
            // 
            // upperThresholdValueToolStripMenuItem
            // 
            this.upperThresholdValueToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.upperThresholdTextBox});
            this.upperThresholdValueToolStripMenuItem.Name = "upperThresholdValueToolStripMenuItem";
            this.upperThresholdValueToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.upperThresholdValueToolStripMenuItem.Text = "Upper threshold value...";
            // 
            // playPauseToolStripMenuItem
            // 
            this.playPauseToolStripMenuItem.Name = "playPauseToolStripMenuItem";
            this.playPauseToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.playPauseToolStripMenuItem.Text = "Play/Pause";
            // 
            // stepForwardToolStripMenuItem
            // 
            this.stepForwardToolStripMenuItem.Name = "stepForwardToolStripMenuItem";
            this.stepForwardToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.stepForwardToolStripMenuItem.Text = "Step forward";
            // 
            // stepBackwardToolStripMenuItem
            // 
            this.stepBackwardToolStripMenuItem.Name = "stepBackwardToolStripMenuItem";
            this.stepBackwardToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.stepBackwardToolStripMenuItem.Text = "Step backward";
            // 
            // jumpForwardToolStripMenuItem
            // 
            this.jumpForwardToolStripMenuItem.Name = "jumpForwardToolStripMenuItem";
            this.jumpForwardToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.jumpForwardToolStripMenuItem.Text = "Jump forward";
            // 
            // jumpBackwardToolStripMenuItem
            // 
            this.jumpBackwardToolStripMenuItem.Name = "jumpBackwardToolStripMenuItem";
            this.jumpBackwardToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.jumpBackwardToolStripMenuItem.Text = "Jump backward";
            // 
            // increaseRateToolStripMenuItem
            // 
            this.increaseRateToolStripMenuItem.Name = "increaseRateToolStripMenuItem";
            this.increaseRateToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.increaseRateToolStripMenuItem.Text = "Increase rate";
            // 
            // decreaseRateToolStripMenuItem
            // 
            this.decreaseRateToolStripMenuItem.Name = "decreaseRateToolStripMenuItem";
            this.decreaseRateToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.decreaseRateToolStripMenuItem.Text = "Decrease rate";
            // 
            // pluginsToolStripMenuItem
            // 
            this.pluginsToolStripMenuItem.Name = "pluginsToolStripMenuItem";
            this.pluginsToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.pluginsToolStripMenuItem.Text = "Plugins";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // pushToPlayShortcutTextBox
            // 
            this.pushToPlayShortcutTextBox.BackColor = System.Drawing.Color.LightGray;
            this.pushToPlayShortcutTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pushToPlayShortcutTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.pushToPlayShortcutTextBox.HighlightColor = System.Drawing.Color.White;
            this.pushToPlayShortcutTextBox.Location = new System.Drawing.Point(118, 126);
            this.pushToPlayShortcutTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.pushToPlayShortcutTextBox.Name = "pushToPlayShortcutTextBox";
            this.pushToPlayShortcutTextBox.Size = new System.Drawing.Size(56, 21);
            this.pushToPlayShortcutTextBox.TabIndex = 20;
            this.pushToPlayShortcutTextBox.TabStop = false;
            this.pushToPlayShortcutTextBox.Text = "None";
            // 
            // decrRateShortcutTextBox
            // 
            this.decrRateShortcutTextBox.BackColor = System.Drawing.Color.LightGray;
            this.decrRateShortcutTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.decrRateShortcutTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.decrRateShortcutTextBox.HighlightColor = System.Drawing.Color.White;
            this.decrRateShortcutTextBox.Location = new System.Drawing.Point(286, 101);
            this.decrRateShortcutTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.decrRateShortcutTextBox.Name = "decrRateShortcutTextBox";
            this.decrRateShortcutTextBox.Size = new System.Drawing.Size(56, 21);
            this.decrRateShortcutTextBox.TabIndex = 16;
            this.decrRateShortcutTextBox.TabStop = false;
            this.decrRateShortcutTextBox.Text = "None";
            // 
            // stepBackShortcutTextBox
            // 
            this.stepBackShortcutTextBox.BackColor = System.Drawing.Color.LightGray;
            this.stepBackShortcutTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stepBackShortcutTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.stepBackShortcutTextBox.HighlightColor = System.Drawing.Color.White;
            this.stepBackShortcutTextBox.Location = new System.Drawing.Point(286, 51);
            this.stepBackShortcutTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.stepBackShortcutTextBox.Name = "stepBackShortcutTextBox";
            this.stepBackShortcutTextBox.Size = new System.Drawing.Size(56, 21);
            this.stepBackShortcutTextBox.TabIndex = 15;
            this.stepBackShortcutTextBox.TabStop = false;
            this.stepBackShortcutTextBox.Text = "None";
            // 
            // jumpFwShortcutTextBox
            // 
            this.jumpFwShortcutTextBox.BackColor = System.Drawing.Color.LightGray;
            this.jumpFwShortcutTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.jumpFwShortcutTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.jumpFwShortcutTextBox.HighlightColor = System.Drawing.Color.White;
            this.jumpFwShortcutTextBox.Location = new System.Drawing.Point(118, 76);
            this.jumpFwShortcutTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.jumpFwShortcutTextBox.Name = "jumpFwShortcutTextBox";
            this.jumpFwShortcutTextBox.Size = new System.Drawing.Size(56, 21);
            this.jumpFwShortcutTextBox.TabIndex = 14;
            this.jumpFwShortcutTextBox.TabStop = false;
            this.jumpFwShortcutTextBox.Text = "None";
            // 
            // incrRateShortcutTextBox
            // 
            this.incrRateShortcutTextBox.BackColor = System.Drawing.Color.LightGray;
            this.incrRateShortcutTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.incrRateShortcutTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.incrRateShortcutTextBox.HighlightColor = System.Drawing.Color.White;
            this.incrRateShortcutTextBox.Location = new System.Drawing.Point(118, 101);
            this.incrRateShortcutTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.incrRateShortcutTextBox.Name = "incrRateShortcutTextBox";
            this.incrRateShortcutTextBox.Size = new System.Drawing.Size(56, 21);
            this.incrRateShortcutTextBox.TabIndex = 13;
            this.incrRateShortcutTextBox.TabStop = false;
            this.incrRateShortcutTextBox.Text = "None";
            // 
            // jumpBackShortcutTextBox
            // 
            this.jumpBackShortcutTextBox.BackColor = System.Drawing.Color.LightGray;
            this.jumpBackShortcutTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.jumpBackShortcutTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.jumpBackShortcutTextBox.HighlightColor = System.Drawing.Color.White;
            this.jumpBackShortcutTextBox.Location = new System.Drawing.Point(286, 76);
            this.jumpBackShortcutTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.jumpBackShortcutTextBox.Name = "jumpBackShortcutTextBox";
            this.jumpBackShortcutTextBox.Size = new System.Drawing.Size(56, 21);
            this.jumpBackShortcutTextBox.TabIndex = 12;
            this.jumpBackShortcutTextBox.TabStop = false;
            this.jumpBackShortcutTextBox.Text = "None";
            // 
            // stepFwShortcutTextBox
            // 
            this.stepFwShortcutTextBox.BackColor = System.Drawing.Color.LightGray;
            this.stepFwShortcutTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stepFwShortcutTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.stepFwShortcutTextBox.HighlightColor = System.Drawing.Color.White;
            this.stepFwShortcutTextBox.Location = new System.Drawing.Point(118, 51);
            this.stepFwShortcutTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.stepFwShortcutTextBox.Name = "stepFwShortcutTextBox";
            this.stepFwShortcutTextBox.Size = new System.Drawing.Size(56, 21);
            this.stepFwShortcutTextBox.TabIndex = 11;
            this.stepFwShortcutTextBox.TabStop = false;
            this.stepFwShortcutTextBox.Text = "None";
            // 
            // playShortcutTextBox
            // 
            this.playShortcutTextBox.BackColor = System.Drawing.Color.LightGray;
            this.playShortcutTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playShortcutTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.playShortcutTextBox.HighlightColor = System.Drawing.Color.White;
            this.playShortcutTextBox.Location = new System.Drawing.Point(118, 26);
            this.playShortcutTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.playShortcutTextBox.Name = "playShortcutTextBox";
            this.playShortcutTextBox.Size = new System.Drawing.Size(56, 21);
            this.playShortcutTextBox.TabIndex = 10;
            this.playShortcutTextBox.TabStop = false;
            this.playShortcutTextBox.Text = "None";
            // 
            // saveShortcutTextBox
            // 
            this.saveShortcutTextBox.BackColor = System.Drawing.Color.LightGray;
            this.saveShortcutTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saveShortcutTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.saveShortcutTextBox.HighlightColor = System.Drawing.Color.White;
            this.saveShortcutTextBox.Location = new System.Drawing.Point(286, 26);
            this.saveShortcutTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.saveShortcutTextBox.Name = "saveShortcutTextBox";
            this.saveShortcutTextBox.Size = new System.Drawing.Size(56, 21);
            this.saveShortcutTextBox.TabIndex = 9;
            this.saveShortcutTextBox.TabStop = false;
            this.saveShortcutTextBox.Text = "None";
            // 
            // addAnnotationButton
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.addAnnotationButton, 2);
            this.addAnnotationButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addAnnotationButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.addAnnotationButton.Location = new System.Drawing.Point(178, 153);
            this.addAnnotationButton.MaximumSize = new System.Drawing.Size(158, 39);
            this.addAnnotationButton.MinimumSize = new System.Drawing.Size(158, 39);
            this.addAnnotationButton.Name = "addAnnotationButton";
            this.addAnnotationButton.Size = new System.Drawing.Size(158, 39);
            this.addAnnotationButton.TabIndex = 18;
            this.addAnnotationButton.TabStop = false;
            this.addAnnotationButton.Text = "Add annotation key";
            this.addAnnotationButton.UseVisualStyleBackColor = true;
            this.addAnnotationButton.Click += new System.EventHandler(this.addAnnotationButton_Click);
            // 
            // dataColumnComboBox
            // 
            this.dataColumnComboBox.Name = "dataColumnComboBox";
            this.dataColumnComboBox.Size = new System.Drawing.Size(121, 23);
            // 
            // lowerThresholdTextBox
            // 
            this.lowerThresholdTextBox.BackColor = System.Drawing.Color.LightGreen;
            this.lowerThresholdTextBox.Name = "lowerThresholdTextBox";
            this.lowerThresholdTextBox.Size = new System.Drawing.Size(100, 23);
            this.lowerThresholdTextBox.Text = "0.0";
            // 
            // upperThresholdTextBox
            // 
            this.upperThresholdTextBox.BackColor = System.Drawing.Color.LightGreen;
            this.upperThresholdTextBox.Name = "upperThresholdTextBox";
            this.upperThresholdTextBox.Size = new System.Drawing.Size(100, 23);
            this.upperThresholdTextBox.Text = "0.0";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1115, 926);
            this.Controls.Add(this.splitContainer2);
            this.Controls.Add(this.menuStrip1);
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Annotator";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.positionTrackBar)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TrackBar positionTrackBar;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label playStateLabel;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private Annotator.Controls.ShortcutTextBox decrRateShortcutTextBox;
        private Annotator.Controls.ShortcutTextBox stepBackShortcutTextBox;
        private Annotator.Controls.ShortcutTextBox jumpFwShortcutTextBox;
        private Annotator.Controls.ShortcutTextBox incrRateShortcutTextBox;
        private Annotator.Controls.ShortcutTextBox jumpBackShortcutTextBox;
        private Annotator.Controls.ShortcutTextBox stepFwShortcutTextBox;
        private Annotator.Controls.ShortcutTextBox playShortcutTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private Annotator.Controls.ShortcutTextBox saveShortcutTextBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playbackToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem playPauseToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem stepForwardToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem stepBackwardToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem jumpForwardToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem jumpBackwardToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem increaseRateToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem decreaseRateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataSourcesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importPreferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportPreferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearAnnotationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAnnotationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeLastAnnotationToolStripMenuItem;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private Controls.ShortcutTextBox pushToPlayShortcutTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label annotationKeysLabel;
        private Controls.KeyInputButton addAnnotationButton;
        private System.Windows.Forms.ToolStripMenuItem clearWorkspaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadAnnotationsToolStripMenuItem;
        private ListViewEmbeddedControls.ListViewEx annotationsListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ToolStripMenuItem skipToToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem dataColumnToolStripMenuItem;
        private Annotator.Controls.ToolStripUpdateableComboBox dataColumnComboBox;
        private System.Windows.Forms.ToolStripMenuItem lowerThresholdValueToolStripMenuItem;
        private Annotator.Controls.ToolStripNumericTextBox lowerThresholdTextBox;
        private System.Windows.Forms.ToolStripMenuItem upperThresholdValueToolStripMenuItem;
        private Annotator.Controls.ToolStripNumericTextBox upperThresholdTextBox;
        internal System.Windows.Forms.ToolStripMenuItem pluginsToolStripMenuItem;
        internal System.Windows.Forms.TextBox sessionLogTextBox;

    }
}

