﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Annotator.Forms
{
    public partial class ValueSelectorBox : Form
    {
        public ValueSelectorBox(string title, string messageText, string[] options)
        {
            InitializeComponent();
            this.Text = title;
            this.label1.Text = messageText;
            this.comboBox1.Items.AddRange(options);
            comboBox1.SelectedIndex = 0;
        }

        public String SelectedValue
        {
            get
            {
                return (String)comboBox1.SelectedItem;
            }
        }
    }
}
