﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.Globalization;
using Annotator.Controllers;
using Annotator.Controls;
using Annotator.Utility;
using Annotator.Annotations;

namespace Annotator.Forms
{
    public partial class MainForm : Form
    {

        public const string ANNOTATION_REMOVED_PROMPT_STRING = "Are you sure you want to remove annotation \"{0}\" ({1})?";
        public const int VERTICAL_SCROLL_WIDTH = 17;

        private readonly MainController controller;
        private readonly DataForm dataForm;
        private readonly HashSet<DescriptionTextBox> descriptionTextBoxes = new HashSet<DescriptionTextBox>();

        internal DataForm DataForm
        {
            get
            {
                return dataForm;
            }
        }

        public MainForm()
        {
            InitializeComponent();
            
            //the main controller's constructor and all its children constructors never call into this class, so this is ok
            controller = new MainController(this);
            dataForm = new DataForm(controller);
            button1.Click += new EventHandler(controller.DecreaseRate);
            decreaseRateToolStripMenuItem.Click += new EventHandler(controller.DecreaseRate);
            button2.Click += new EventHandler(controller.JumpBackward);
            jumpBackwardToolStripMenuItem.Click += new EventHandler(controller.JumpBackward);
            button3.Click += new EventHandler(controller.StepBackward);
            stepBackwardToolStripMenuItem.Click += new EventHandler(controller.StepBackward);
            button4.Click += new EventHandler(controller.PlayPause);
            playPauseToolStripMenuItem.Click += new EventHandler(controller.PlayPause);
            button5.Click += new EventHandler(controller.StepForward);
            stepForwardToolStripMenuItem.Click += new EventHandler(controller.StepForward);
            button6.Click += new EventHandler(controller.JumpForward);
            jumpForwardToolStripMenuItem.Click += new EventHandler(controller.JumpForward);
            button7.Click += new EventHandler(controller.IncreaseRate);
            increaseRateToolStripMenuItem.Click += new EventHandler(controller.IncreaseRate);
            positionTrackBar.ValueChanged += new EventHandler(controller.SeekHandler);
            positionTrackBar.MouseDown += new MouseEventHandler(controller.ValidateSeek);
            positionTrackBar.MouseUp += new MouseEventHandler(controller.InvalidateSeek);
            decrRateShortcutTextBox.ShortcutChanged += controller.ShortcutChangedEventHandler;
            decrRateShortcutTextBox.Tag = ShortcutKeyType.DecreaseRate;
            incrRateShortcutTextBox.ShortcutChanged += controller.ShortcutChangedEventHandler;
            incrRateShortcutTextBox.Tag = ShortcutKeyType.IncreaseRate;
            saveShortcutTextBox.ShortcutChanged += controller.ShortcutChangedEventHandler;
            saveShortcutTextBox.Tag = ShortcutKeyType.SaveAnnotations;
            playShortcutTextBox.ShortcutChanged += controller.ShortcutChangedEventHandler;
            playShortcutTextBox.Tag = ShortcutKeyType.PlayPause;
            jumpBackShortcutTextBox.ShortcutChanged += controller.ShortcutChangedEventHandler;
            jumpBackShortcutTextBox.Tag = ShortcutKeyType.JumpBackward;
            jumpFwShortcutTextBox.ShortcutChanged += controller.ShortcutChangedEventHandler;
            jumpFwShortcutTextBox.Tag = ShortcutKeyType.JumpForward;
            stepBackShortcutTextBox.ShortcutChanged += controller.ShortcutChangedEventHandler;
            stepBackShortcutTextBox.Tag = ShortcutKeyType.StepBackward;
            stepFwShortcutTextBox.ShortcutChanged += controller.ShortcutChangedEventHandler;
            stepFwShortcutTextBox.Tag = ShortcutKeyType.StepForward;
            pushToPlayShortcutTextBox.ShortcutChanged += controller.ShortcutChangedEventHandler;
            pushToPlayShortcutTextBox.Tag = ShortcutKeyType.PushToPlay;

            saveAnnotationsToolStripMenuItem.Click += controller.SaveAnnotations;

            controller.InitializeDataBindings();
            dataForm.Show();
        }

        public void AddVideoPanel(Control p)
        {
            flowLayoutPanel1.Controls.Add(p);
        }

        public void SetSessionLogText(string text)
        {
            sessionLogTextBox.Text = text;
            sessionLogTextBox.SelectionStart = sessionLogTextBox.Text.Length;
            sessionLogTextBox.ScrollToCaret();
        }

        public void SetPlayLabelText(string text)
        {
            playStateLabel.Text = text;
        }

        public void StartStopTimer()
        {
            timer1.Enabled = !timer1.Enabled;
        }

        public void RedrawVideos()
        {
            flowLayoutPanel1.Invalidate(true);
        }

        public void SetTrackBarBoundValues(int min, int max)
        {
            positionTrackBar.Minimum = min;
            positionTrackBar.Maximum = max;
        }

        public void SetTrackBarValue(int current)
        {
            /*if (positionTrackBar.Maximum < current)
            {
                positionTrackBar.Maximum = current;
            }*/
            positionTrackBar.Value = current;
        }

        public int GetTrackBarValue()
        {
            return positionTrackBar.Value;
        }

        [DebuggerStepThrough]
        private void timer1_Tick(object sender, EventArgs e)
        {
            controller.Update();
        }

        internal void MakePauseButton()
        {
            button4.Text = "||";
        }

        internal void MakePlayButton()
        {
            button4.Text = "|>";
        }

        internal void DefocusShortcutButtons()
        {
            //playShortcutTextBox.LoseFocus();
            //saveShortcutTextBox.LoseFocus();
            //stepFwShortcutTextBox.LoseFocus();
            //stepBackShortcutTextBox.LoseFocus();
            //jumpFwShortcutTextBox.LoseFocus();
            //jumpBackShortcutTextBox.LoseFocus();
            //incrRateShortcutTextBox.LoseFocus();
            //decrRateShortcutTextBox.LoseFocus();
            //pushToPlayShortcutTextBox.LoseFocus();

            //make all control buttons lose focus
            addAnnotationButton.Focus();

            //doing this.Focus() will not remove focus from a ShortcutTextBox for some reason
        }

        internal void ApplyTextToShortcutButtons(ShortcutController ctrl)
        {
            playShortcutTextBox.SetShortcut(ctrl.GetKeyForAction(ShortcutKeyType.PlayPause));
            saveShortcutTextBox.SetShortcut(ctrl.GetKeyForAction(ShortcutKeyType.SaveAnnotations));
            stepFwShortcutTextBox.SetShortcut(ctrl.GetKeyForAction(ShortcutKeyType.StepForward));
            stepBackShortcutTextBox.SetShortcut(ctrl.GetKeyForAction(ShortcutKeyType.StepBackward));
            jumpFwShortcutTextBox.SetShortcut(ctrl.GetKeyForAction(ShortcutKeyType.JumpForward));
            jumpBackShortcutTextBox.SetShortcut(ctrl.GetKeyForAction(ShortcutKeyType.JumpBackward));
            incrRateShortcutTextBox.SetShortcut(ctrl.GetKeyForAction(ShortcutKeyType.IncreaseRate));
            decrRateShortcutTextBox.SetShortcut(ctrl.GetKeyForAction(ShortcutKeyType.DecreaseRate));
            pushToPlayShortcutTextBox.SetShortcut(ctrl.GetKeyForAction(ShortcutKeyType.PushToPlay));
        }

        private void removeAnnotationControlSet(int row, bool userInput = true)
        {
            int i, lim;
            Control ctrl;
            ShortcutTextBox sh;
            Keys k;
            lim = tableLayoutPanel3.RowCount - 1;
            tableLayoutPanel3.Controls.Remove(ctrl = tableLayoutPanel3.GetControlFromPosition(0, row));
            ctrl.Click -= removeAnnotationButton_Click;
            ctrl.Dispose();
            tableLayoutPanel3.Controls.Remove(ctrl = tableLayoutPanel3.GetControlFromPosition(1, row));
            ctrl.TextChanged -= annotation_TextChanged;
            ctrl.LostFocus -= ctrl_LostFocus;
            descriptionTextBoxes.Remove(ctrl as DescriptionTextBox);
            ctrl.Dispose();
            tableLayoutPanel3.Controls.Remove(ctrl = tableLayoutPanel3.GetControlFromPosition(4, row));
            sh = (ShortcutTextBox)ctrl;
            sh.ShortcutChanged -= annotation_ShortcutChanged;
            k = sh.Shortcut;
            ctrl.Dispose();

            for (i = row; i < lim; i++)
            {
                ctrl = tableLayoutPanel3.GetControlFromPosition(0, i + 1);
                if (ctrl != null)
                {
                    tableLayoutPanel3.SetCellPosition(ctrl, new TableLayoutPanelCellPosition(0, i));
                }
                ctrl = tableLayoutPanel3.GetControlFromPosition(1, i + 1);
                if (ctrl != null)
                {
                    tableLayoutPanel3.SetCellPosition(ctrl, new TableLayoutPanelCellPosition(1, i));
                }
                ctrl = tableLayoutPanel3.GetControlFromPosition(3, i + 1);
                if (ctrl != null)
                {
                    tableLayoutPanel3.SetCellPosition(ctrl, new TableLayoutPanelCellPosition(3, i));
                }
                ctrl = tableLayoutPanel3.GetControlFromPosition(4, i + 1);
                if (ctrl != null)
                {
                    tableLayoutPanel3.SetCellPosition(ctrl, new TableLayoutPanelCellPosition(4, i));
                }
            }
            if (userInput)
            {
                controller.RemoveAnnotationKeyFromAnnotator(k);
            }
        }

        private void addAnnotationControlSet(int row, Keys k = Keys.None, string description = "Enter annotation description...", bool userInput = true)
        {
            var closeBtn = new Button();
            var txtBox = new DescriptionTextBox();
            var shortcutBox = new ShortcutTextBox();

            closeBtn.Text = "X";
            closeBtn.Font = new Font(addAnnotationButton.Font.Name, 7, FontStyle.Bold);
            closeBtn.Size = new Size(21, 21);
            closeBtn.MaximumSize = closeBtn.Size;
            closeBtn.MinimumSize = closeBtn.Size;
            closeBtn.Margin = new System.Windows.Forms.Padding(3);
            closeBtn.Dock = DockStyle.Fill;
            //closeBtn.Location = new Point(0, 0);

            txtBox.Text = description;
            txtBox.MinimumSize = closeBtn.Size;
            txtBox.Font = addAnnotationButton.Font;
            //txtBox.Margin = new System.Windows.Forms.Padding(24, 0,0,0);
            //txtBox.Location = new Point(24, 0);
            txtBox.Dock = DockStyle.Fill;
            descriptionTextBoxes.Add(txtBox);

            shortcutBox.SetShortcut(k);
            shortcutBox.Dock = DockStyle.Fill;

            closeBtn.Click += removeAnnotationButton_Click;
            txtBox.TextChanged += annotation_TextChanged;
            shortcutBox.ShortcutChanged += annotation_ShortcutChanged;
            txtBox.LostFocus += ctrl_LostFocus;

            tableLayoutPanel3.Controls.Add(closeBtn, 0, row);
            tableLayoutPanel3.Controls.Add(txtBox, 1, row);
            tableLayoutPanel3.SetColumnSpan(txtBox, 3);
            tableLayoutPanel3.Controls.Add(shortcutBox, 4, row);
            tableLayoutPanel3.ScrollControlIntoView(closeBtn);
            if (userInput)
            {
                controller.AddAnnotationKeyToAnnotator(shortcutBox.Shortcut, txtBox.Text);
            }
        }

        private void removeAnnotationButton_Click(object sender, EventArgs e)
        {
            int row = tableLayoutPanel3.GetRow((Control)sender);
            string description = ((TextBox)tableLayoutPanel3.GetControlFromPosition(1, row)).Text;
            string shortcut = ((TextBox)tableLayoutPanel3.GetControlFromPosition(4, row)).Text;
            if (MessageBox.Show(string.Format(ANNOTATION_REMOVED_PROMPT_STRING, description, shortcut),
                               "Remove annotation?",
                               MessageBoxButtons.YesNo,
                               MessageBoxIcon.Warning
                            ) == DialogResult.Yes)
            {
                RemoveAnnotationOnRow(row);
            }
        }

        private void annotation_TextChanged(object sender, EventArgs e)
        {
            Keys k;
            String desc;
            ShortcutTextBox sh;
            DescriptionTextBox d = (DescriptionTextBox)sender;
            int row;
            row = tableLayoutPanel3.GetRow(d);
            sh = (ShortcutTextBox)tableLayoutPanel3.GetControlFromPosition(4, row);
            k = sh.Shortcut;
            desc = d.Text;
            controller.ChangeAnnotationDescription(k, desc);
        }

        private void annotation_ShortcutChanged(object sender, ShortcutChangedEventArgs e)
        {
            e.Succeeded = controller.ChangeAnnotationShortcut(e.OldShortcut, e.Shortcut);
        }

        private void addAnnotationButton_Click(object sender, EventArgs e)
        {
            AddAnnotationKey();
        }

        private void ctrl_LostFocus(object sender, EventArgs e)
        {
            DefocusShortcutButtons();
        }

        public void RemoveAnnotationOnRow(int row, bool userInput = true)
        {
            removeAnnotationControlSet(row, userInput);
            tableLayoutPanel3.RowCount--;
            tableLayoutPanel3.Size = tableLayoutPanel3.PreferredSize;
        }

        public void RemoveAnnotationKey(Keys k, bool userInput = true)
        {
            int lim = tableLayoutPanel3.RowCount;
            for (int row = tableLayoutPanel3.GetRow(addAnnotationButton) + 1; row < lim; row++)
            {
                ShortcutTextBox sh = tableLayoutPanel3.GetControlFromPosition(4, row) as ShortcutTextBox;
                if (sh.Shortcut == k)
                {
                    RemoveAnnotationOnRow(row, userInput);
                    return;
                }
            }
            throw new ArgumentException("Could not find annotation mapped to key " + k.ToString(),"k");
        }

        public void AddAnnotationKey(Keys k = Keys.NoName, string description = null, bool userInput = true)
        {
            int row = tableLayoutPanel3.RowCount;
            tableLayoutPanel3.RowCount = row + 1;

            if (description == null)
            {
                if (k == Keys.NoName)
                {
                    addAnnotationControlSet(row, controller.GetNextAnnotationKey(), userInput: userInput);
                }
                else
                {
                    addAnnotationControlSet(row, k, userInput: userInput);
                }
            }
            else
            {
                if (k == Keys.NoName)
                {
                    addAnnotationControlSet(row, controller.GetNextAnnotationKey(), description, userInput: userInput);
                }
                else
                {
                    addAnnotationControlSet(row, k, description, userInput);
                }
            }

            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.AutoSize));
        }

        public void RemoveAllVideoPanels()
        {
            foreach (Control c in flowLayoutPanel1.Controls)
            {
                c.Dispose();
            }
            flowLayoutPanel1.Controls.Clear();
        }

        public void RemoveVideoPanel(Panel p)
        {
            flowLayoutPanel1.Controls.Remove(p);
            p.Dispose();
        }

        private void dataSourcesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dataForm.Visible == false)
            {
                dataForm.Visible = true;
            }
        }

        private void importPreferencesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Import preferences from file...";
            ofd.Filter = "Ini files|*.ini";
            ofd.DefaultExt = ".ini";
            ofd.CheckFileExists = true;
            ofd.AddExtension = true;
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                controller.LoadPropertiesFromFile(ofd.FileName);
            }
        }

        private void exportPreferencesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Export preferences to file...";
            sfd.Filter = "Ini files|*.ini";
            sfd.DefaultExt = ".ini";
            sfd.CheckPathExists = true;
            sfd.OverwritePrompt = true;
            sfd.AddExtension = true;
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                controller.SavePropertiesToFile(sfd.FileName);
            }
        }

        private void clearAnnotationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to remove all your annotations? All unsaved annotations will be lost.", "Are you sure?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
            {
                controller.ClearAnnotations();
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (AboutBox aboutBox = new AboutBox())
            {
                aboutBox.ShowDialog();
            }
        }

        private void removeLastAnnotationToolStripMenuItem_MouseDown(object sender, MouseEventArgs e)
        {
            controller.RemoveLastAnnotation();
        }

        public void ScrollAnnotationButtonIntoView()
        {
            tableLayoutPanel3.ScrollControlIntoView(addAnnotationButton);
        }

        private void clearWorkspaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controller.ClearAllElements();
        }

        private void loadAnnotationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Title = "Load annotation file...";
                ofd.Filter = "Text and tsv files (*.txt,*.tsv)|*.txt;*.tsv|All files|*.*";
                ofd.DefaultExt = ".txt";
                ofd.CheckFileExists = true;
                ofd.AddExtension = true;
                if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    controller.LoadAnnotationFile(ofd.FileName);
                }
            }
        }

        public bool IsDescriptionBoxFocused()
        {
            foreach (DescriptionTextBox box in descriptionTextBoxes)
            {
                if (box.Focused)
                {
                    return true;
                }
            }
            return false;
        }

        public void AddAnnotation(Annotation annotation)
        {
            ListViewItem item = new ListViewItem(annotation.Key.Key.ToString());
            item.SubItems.Add(new ListViewItem.ListViewSubItem(item, Utils.CreateTimeString(annotation.Time).ToString()));
            item.SubItems.Add(new ListViewItem.ListViewSubItem(item, Utils.CreateSecondStringNoUnit(annotation.Time).ToString()));

            annotationsListView.Items.Add(item);
            var removeBtn = new Button();
            removeBtn.Text = "X";
            removeBtn.Font = new Font(addAnnotationButton.Font.Name, 6, FontStyle.Bold);
            removeBtn.Size = new Size(21, 21);
            removeBtn.Margin = new System.Windows.Forms.Padding(3);
            removeBtn.Tag = new Tuple<ListViewItem, Annotation> (item, annotation);
            item.Tag = new Tuple<Button, Annotation>(removeBtn, annotation);
            removeBtn.Click += closeBtn_Click;
            annotationsListView.AddEmbeddedControl(removeBtn, 3, item.Index);
            item.EnsureVisible();
        }

        void closeBtn_Click(object sender, EventArgs e)
        {
            Tuple<ListViewItem, Annotation> data = (sender as Control).Tag as Tuple<ListViewItem, Annotation>;
            annotationsListView.Items.Remove(data.Item1);
            controller.RemoveAnnotationFromAnnotator(data.Item2);
        }

        public void RemoveAnnotation(Annotation annotation)
        {
            ListViewItem found = null;
            foreach (ListViewItem lvi in annotationsListView.Items)
            {
                if (annotation.Equals((lvi.Tag as Tuple<Button, Annotation>).Item2))
                {
                    found = lvi;
                    break;
                }
            }
            Button removeBtn = (found.Tag as Tuple<Button, Annotation>).Item1;
            removeBtn.Click -= closeBtn_Click;
            removeBtn.Dispose();
            annotationsListView.Items.Remove(found);
        }

        public void ClearAnnotations()
        {
            foreach (ListViewItem lvi in annotationsListView.Items)
            {
                Button removeBtn = (lvi.Tag as Tuple<Button, Annotation>).Item1;
                removeBtn.Click -= closeBtn_Click;
                removeBtn.Dispose();
            }
            annotationsListView.Items.Clear();
        }

        private void dataColumnToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            dataColumnComboBox.UpdatePossibleValues(controller.DataColumnNames);
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SkipToNextValueInRange();
        }

        internal void SkipToNextValueInRange()
        {
            if (!lowerThresholdTextBox.HasNumericValue)
            {
                controller.AddSessionLogData("Cannot skip to next value in range, lower threshold is invalid.");
            }
            else if (!upperThresholdTextBox.HasNumericValue)
            {
                controller.AddSessionLogData("Cannot skip to next value in range, upper threshold is invalid.");
            }
            else if (dataColumnComboBox.SelectedItem == null)
            {
                controller.AddSessionLogData("Cannot skip to next value in range, no data column has been selected.");
            }
            else
            {
                controller.SkipToNextValueInRange(dataColumnComboBox.SelectedItem as string, lowerThresholdTextBox.NumericValue, upperThresholdTextBox.NumericValue);
            }
        }
    }
}