﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Annotator.Forms
{
    public partial class MultiValueSelectorBox : Form
    {
        private readonly Dictionary<string, CheckBox> checkBoxes = new Dictionary<string, CheckBox>(); 

        public MultiValueSelectorBox(string title, string messageText, string[] options)
        {
            InitializeComponent();
            this.Text = title;
            this.label1.Text = messageText;
            foreach (var x in options)
            {
                AddCheckBox(x);
            }
        }

        private void AddCheckBox(string text)
        {
            CheckBox cb = new CheckBox();
            checkBoxes.Add(text, cb);
            cb.Text = text;
            cb.Margin = new Padding(3);
            flowLayoutPanel1.Controls.Add(cb);
        }

        public bool IsChecked(string key)
        {
            return checkBoxes.ContainsKey(key) && checkBoxes[key].Checked;
        }

    }
}
