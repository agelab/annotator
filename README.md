# Annotator #

## What does it do? ##

This tool was created by the [MIT AgeLab](http://agelab.mit.edu) to enable efficient analysis and annotation of audio, video and time series files. It supports simultaneous synchronized playback of tens of videos, while also plotting any number of time series drawn from tab-separated data files. The user can then use a customizable set of keys to mark important events in the data for further analysis.

## Getting started ##

First, ensure that your system has up-to-date video and audio codecs. Annotator depends on these codecs being available on your system in order to correctly play back multimedia files.

Then, download, unzip and install the most recent Annotator version the Downloads section. The installer should automatically take care of any unmet dependencies as long as you have an Internet connection. When the installation completes, the Annotator will start automatically.

To import files, simply drag and drop them onto the Annotator interface. You can then use the buttons on the Annotator interface to control playback, add annotations, manage data files or change the synchronization offsets (in case data timestamps are out of sync).

Here is a list of the default playback shortcuts:
* Play/pause toggle: space bar
* Frame step forward: right arrow
* Frame step backward: left arrow
* Jump 1s forward: up arrow
* Jump 1s backward: down arrow
* Increase playback rate: ] key
* Decrease playback rate: [ key
* Hold-to-play button: P key

## Plugin API ##

The Annotator has a plugin API that allows new functionality to be added easily. Plugins are automatically loaded on startup from the `plugins` directory inside the installation directory of the Annotator. See the `AutoSearchPathsPlugin` directory in this project for an example plugin.

## Contributing to the project ##

If you want to add functionality to the Annotator, please fork the repository, test your new functionality (preferably also adding unit tests) and then submit a pull request. We will review your request and respond as soon as possible. Code that does not conform to the code style of the Annotator repository will be rejected by default.

If you find a bug, to make the process easier, please use the following format to report it:

~~~~
Component:
Version and Build:
Operating system:
Codecs:
Bug description:
Steps to reproduce:
Expected result:
Actual result:
Additional information:
~~~~

Here is a sample bug report that follows this format:
~~~~
Component: Annotator
Version and Build: 1.4.0.0
Operating system: Windows 7 64-bit Service Pack 1
Codecs: K-Lite Codec Pack Standard v10.5.0

Bug description:
Playback of a particular video file makes Annotator crash.
Windows Media Player can play back the same video successfully.

Steps to reproduce:
1. drag-and-drop the file "video.mp4" (attached to this report) onto Annotator
2. press the play button
3. observe Annotator crash

Expected result: Annotator plays the video back successfully.
Actual result: Annotator crashes as soon as the play button is pressed.
Additional information: none
~~~~

After you have created such a bug report, please email it to [agelab-annotator-bugs@mit.edu](mailto:agelab-annotator-bugs@mit.edu) along with any files necessary to reproduce the bug. Bug reports that do not contain steps to reproduce the bug will almost certainly not be acted upon.

## Contact ##

This project was developed by the [MIT AgeLab](http://agelab.mit.edu). You can contact the project owners here: [agelab-annotator-admin@mit.edu](mailto:agelab-annotator-admin@mit.edu)

## License ##

BSD 3-clause, see LICENSE.txt

## Dependencies ##

We would like to thank the following open-source projects:

* [OxyPlot](http://oxyplot.org/)
* [DirectShow.NET](http://directshownet.sourceforge.net/)
* [Embedding Controls in a ListView](http://www.codeproject.com/Articles/9188/Embedding-Controls-in-a-ListView)