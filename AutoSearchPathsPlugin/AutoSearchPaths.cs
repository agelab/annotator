﻿using Annotator.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace AutoSearchPathsPlugin
{
    [Export(typeof(IPlugin))]
    [ExportMetadata("Name", "Auto Search Paths")]
    public class AutoSearchPaths : IPlugin
    {
        private IPluginHost host;
        private string searchPathsFile;
        private HashSet<string> searchPaths;
        private bool modified = false;

        #region IPlugin Members

        public void Initialize(IPluginHost host)
        {
            this.host = host;
            searchPathsFile = Path.Combine(host.GetPluginDirectoryPath(), "searchPaths.txt");
            if(File.Exists(searchPathsFile))
            {
                string[] p = File.ReadAllLines(searchPathsFile);
                searchPaths = new HashSet<string>(p);
                foreach(string x in p)
                {
                    host.AddSearchPath(x);
                }
            }
            else
            {
                searchPaths = new HashSet<string>();
            }
        }

        public void OnSavingAnnotations(IAnnotationFile data)
        {
            //no-op
        }

        public void OnSavedAnnotations(IAnnotationFile data, string filepath)
        {
            foreach(var x in data.TimelineElements)
            {
                modified |= searchPaths.Add(Path.GetDirectoryName(x.Filepath));
            }
        }

        public void OnApplicationClosed()
        {
            foreach (var x in host.GetSearchPaths())
            {
                modified |= searchPaths.Add(x);
            }
            if(modified)
            {
                File.WriteAllLines(searchPathsFile, searchPaths);
            }
        }

        #endregion
    }
}
